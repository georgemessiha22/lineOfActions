# lineOfActions
German University in Cairo
Media Engineering and Technology
Prof. Dr.Slim Abdennadher
Assoc. Prof. Georg Jung
, Spring term 2013
Computer Programming Lab
Lines of Action Game Description
1
Introduction
Lines of Action is a two player strategy board game where each player aims to connect all his/her checkers
into one group. The board is a standard 8 × 8 chessboard. The game starts with each player having 12
checkers. The black checkers are placed horizontally on the two rows along the top and bottom of the
board, while the white checkers are placed vertically on the two columns at the left and right of the
board. The 2 players alternate at moving their checkers in the three lines of action: vertical, horizontal
and diagonal. The rst player to get all her/his checkers into a single connected group wins.
Lines of Action
2
Description
A standard game/round of Lines of Action can be described as follows:
• A standard board of 8 rows and 8 columns
• Two kinds of pieces:
1. 12 white (player 1)
2. 12 black (player 2)
• Initially the board is set as shown in the gure above.
12.1
2.1.1
Rules
Moving Checkers
1. Players alternate turns. The white moves rst.
2. Checkers move in the 3 lines of action: horizontally, vertically, or diagonally.
3. The number of squares moved by a checker must be exaclty the same as the number of checkers
(belonging to both players) in that line of action.
4. A checker belonging to a player may jump over other checkers belonging to that player, but it may
not jump over checkers belonging to the opponent.
5. A checker captures an opponent's checker by landing on it.
2.1.2
Game Over
1. The rst player to connect all her/his checkers into a single group is the winner. For example, in the
case below, the white player wins.
Game over scenario
2. If one player is reduced by captures to a single piece, that is a win for the captured player.
3. If a move lead to both players being winners, the player who made that move is the winner.
3
Requirements
Each team is required to implement both a back-end engine for Lines of Action and a Graphical User
Interface (GUI) to interact with that engine (play the game). The implementation has to satisfy the
following:
• The package convention to use is eg.edu.guc.loa.engine for all engine related les,
and eg.edu.guc.loa.gui for all GUI related les.
• Initializing the back-end for the game has to be through a class named Board , that implements the
interface BoardInterface . The class and interface must both be placed in eg.edu.guc.loa.engine .
More details about this interface in Section 3.1.
• The class Board has to provide an empty constructor.
2• Being consistent with the game over conditions provided in Section 2.
• Graceful handling of exceptions.
3.1
Board Interface
package eg.guc.edu.loa.engine;
public interface BoardInterface {
boolean move(Point start, Point end);
boolean isGameOver();
int getWinner();
int getColor(Point p);
int getTurn();
ArrayList<Point> getPossibleMoves(Point start);
}
Methods' overview:
• boolean move(Point start, Point end) : if it is a valid move, the board should change accor-
dingly, and the method returns true, otherwise it returns false.
• In general, the white player is refered to by 1 and the black player is refered to by 2.
• int getColor(int row, int col) : returns 1 if the checker belongs to the white player, 2 if it
belongs to the black player and 0 if the cell is empty.
4
GUI Requirements
You are required to produce a playable graphical interface for Lines of Action. In this interface, all
the actions described in BoardInterface (see Section 3.1) must be possible, along with the needed
requirements mentioned above. Moreover, when a player clicks on a checker, the valid places it could
jump to should be shown. Feel free to be creative and extend the requirements with any extra features.
5
Bonus
• Networking
• AI
For those who would like to go into AI, check this paper :)
http://www.mariogarcia.info/portfolio/LOA/AI_LinesOfAction.pdf
6
Where to play
You can play the game online at http://www.smartestgames.com/game/lines-of-action/
3
