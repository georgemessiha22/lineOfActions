package eg.edu.guc.loa.GUI.stuff;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import eg.edu.guc.loa.GUI.MainController;
import eg.edu.guc.loa.engine.Player;

@SuppressWarnings("serial")
public class BoardOptions extends JPanel {
	private MainController mainController;
	private ColorList color1Option;
	private ColorList color2Option;
	private ColorList possibleMoveColor;
	private ColorList SelectionColor;
	private CheckerButton selection;
	private CheckerButton colorPossible;
	private CheckerButton color1;
	private CheckerButton color2;
	private JComboBox<String> options;
	private JPanel s;
	private String[] names = { "Left", "Right" };
	private JPanel op;

	public BoardOptions(MainController mainController) {
		this.mainController = mainController;
		this.setOpaque(false);
		this.setLayout(new GridLayout(1, 0));
		addFirstColor();
		this.add(new PlainJButton("  "));
		addSecondColor();
		this.add(new PlainJButton("  "));
		addPossibleColor();
		this.add(new PlainJButton("  "));
		addChoosenColor();
		this.add(new PlainJButton("  "));
		addSideSelection();
	}

	private void addChoosenColor() {
		JPanel temp = new JPanel(new BorderLayout());
		temp.setOpaque(false);
		selection = new CheckerButton(this.mainController.getMainPanel()
				.getCheckersPanel().getColor2(), new Player("C",this.mainController.getMainPanel()
				.getCheckersPanel().getSelectedColor()));
		selection.setEnabled(false);
		selection.setFocusable(false);
		selection.setToolTipText("Selection Color");
		SelectionColor = new ColorList(this.mainController.getMainPanel()
				.getCheckersPanel().getDefaultSelected());
		SelectionColor.setSelected(this.mainController.getMainPanel()
				.getCheckersPanel().getSelectedColor());
		SelectionColor.addActionListener(new theHandler());
		temp.add(SelectionColor, BorderLayout.NORTH);
		temp.add(selection, BorderLayout.CENTER);
		this.add(temp);
	}

	private void addSideSelection() {
		options = new JComboBox<String>(names);
		options.setOpaque(false);
		op = new JPanel(new BorderLayout());
		op.setOpaque(false);
		this.s = new JPanel(new BorderLayout());
		if (this.mainController.getMainPanel().getPosition() == 1) {
			if (this.mainController.getBackground() == Color.red) {
				this.s.setBorder(BorderFactory.createMatteBorder(1, 20, 1, 1,
						Color.BLACK));
			} else {
				this.s.setBorder(BorderFactory.createMatteBorder(1, 20, 1, 1,
						Color.RED));
			}
		} else {
			if (this.mainController.getMainPanel().getPosition() == 2) {
				if (this.mainController.getBackground() == Color.red) {
					this.s.setBorder(BorderFactory.createMatteBorder(1, 1, 1,
							20, Color.BLACK));
				} else {
					this.s.setBorder(BorderFactory.createMatteBorder(1, 1, 1,
							20, Color.RED));
				}
			}
		}
		this.options.addActionListener(new theHandler());
		this.s.setBackground(Color.gray);
		this.s.setToolTipText("Position of the Panel");
		op.add(options, BorderLayout.NORTH);
		op.add(this.s, BorderLayout.CENTER);
		options.addActionListener(new theHandler());
		this.add(op);
	}

	private void addFirstColor() {
		JPanel temp = new JPanel(new BorderLayout());
		temp.setOpaque(false);
		color1 = new CheckerButton(this.mainController.getMainPanel()
				.getCheckersPanel().getColor1(), new Player("C",
				this.mainController.getMainPanel().getCheckersPanel()
						.getColor2()));
		color1.setEnabled(false);
		color1.setFocusable(false);
		color1.setToolTipText("Board Color 1");
		color1Option = new ColorList(this.mainController.getMainPanel()
				.getCheckersPanel().getDefaultColor());
		color1Option.setSelected(this.mainController.getMainPanel()
				.getCheckersPanel().getColor1());
		color1Option.addActionListener(new theHandler());
		temp.add(color1Option, BorderLayout.NORTH);
		temp.add(color1, BorderLayout.CENTER);
		this.add(temp);
	}

	private void addPossibleColor() {
		JPanel temp = new JPanel(new BorderLayout());
		temp.setOpaque(false);
		colorPossible = new CheckerButton(this.mainController.getMainPanel()
				.getCheckersPanel().getDefaultColor(), new Player("C",this.mainController.getMainPanel()
						.getCheckersPanel().getPossibleMoveColor()));
		colorPossible.setEnabled(false);
		colorPossible.setFocusable(false);
		colorPossible.setToolTipText("Possible Move Color");
		possibleMoveColor = new ColorList(this.mainController.getMainPanel()
				.getCheckersPanel().getDefaultPossibleMoveColor());
		possibleMoveColor.setSelected(this.mainController.getMainPanel()
				.getCheckersPanel().getPossibleMoveColor());
		possibleMoveColor.addActionListener(new theHandler());
		temp.add(possibleMoveColor, BorderLayout.NORTH);
		temp.add(colorPossible, BorderLayout.CENTER);
		this.add(temp);
	}

	private void addSecondColor() {
		JPanel temp = new JPanel(new BorderLayout());
		temp.setOpaque(false);
		color2 = new CheckerButton(this.mainController.getMainPanel()
				.getCheckersPanel().getColor2(), new Player("C",
				this.mainController.getMainPanel().getCheckersPanel()
						.getColor1()));
		color2.setEnabled(false);
		color2.setFocusable(false);
		color2.setToolTipText("Board Color 2");
		color2Option = new ColorList(this.mainController.getMainPanel()
				.getCheckersPanel().getDefaultColor1());
		color2Option.setSelected(this.mainController.getMainPanel()
				.getCheckersPanel().getColor2());
		color2Option.addActionListener(new theHandler());
		temp.add(color2Option, BorderLayout.NORTH);
		temp.add(color2, BorderLayout.CENTER);
		this.add(temp);
	}

	public Color getColor1() {
		return color1Option.getSelected();
	}

	public Color getColor2() {
		return color2Option.getSelected();
	}

	public Color getPossibleMoveColor() {
		return possibleMoveColor.getSelected();
	}

	public Color getSelectionColor() {
		return SelectionColor.getSelected();
	}
	
	public void reset() {
		this.color1.setPlayerButton(new Player("C",
				this.mainController.getMainPanel().getCheckersPanel()
						.getColor2()));
		this.color1.setBackground(this.mainController.getMainPanel()
				.getCheckersPanel().getColor1());
		this.color1Option.setSelected(this.mainController.getMainPanel()
				.getCheckersPanel().getColor1());
		this.color2.setPlayerButton(new Player("C",
				this.mainController.getMainPanel().getCheckersPanel()
						.getColor1()));
		this.color2.setBackground(this.mainController.getMainPanel()
				.getCheckersPanel().getColor2());
		this.color2Option.setSelected(this.mainController.getMainPanel()
				.getCheckersPanel().getColor2());
		this.colorPossible.setBackground(this.mainController.getMainPanel()
				.getCheckersPanel().getDefaultColor());
		this.colorPossible.setPlayerButton(new Player("C",this.mainController.getMainPanel()
				.getCheckersPanel().getPossibleMoveColor()));
		possibleMoveColor.setSelected(this.mainController.getMainPanel()
				.getCheckersPanel().getPossibleMoveColor());
		this.selection.setPlayerButton(new Player("C",this.mainController.getMainPanel()
				.getCheckersPanel().getSelectedColor()));
		this.selection.setBackground(this.mainController.getMainPanel()
				.getCheckersPanel().getColor2());
		this.SelectionColor.setSelected(this.mainController.getMainPanel()
				.getCheckersPanel().getSelectedColor());
		if (this.mainController.getMainPanel().getPosition() == 1) {
			if (this.mainController.getBackground() == Color.red) {
				this.s.setBorder(BorderFactory.createMatteBorder(1, 20, 1, 1,
						Color.BLACK));
			} else {
				this.s.setBorder(BorderFactory.createMatteBorder(1, 20, 1, 1,
						Color.RED));
			}
		} else {
			if (this.mainController.getMainPanel().getPosition() == 2) {
				if (this.mainController.getBackground() == Color.red) {
					this.s.setBorder(BorderFactory.createMatteBorder(1, 1, 1,
							20, Color.BLACK));
				} else {
					this.s.setBorder(BorderFactory.createMatteBorder(1, 1, 1,
							20, Color.RED));
				}
			}
		}
		this.options.setSelectedIndex(this.mainController.getMainPanel().getPosition()-1);
	}

	private void colorChanged() {
		this.color1
				.setPlayerButton(new Player("l", color2Option.getSelected()));
		this.color2
				.setPlayerButton(new Player("l", color1Option.getSelected()));
		this.color1.setBackground(color1Option.getSelected());
		this.color2.setBackground(color2Option.getSelected());
		this.colorPossible.setPlayerButton(new Player(("C"),this.possibleMoveColor.getSelected()));
		this.selection.setPlayerButton(new Player(("C"),this.SelectionColor.getSelected()));
		this.colorPossible.setBackground(this.color1Option.getSelected());
		this.selection.setBackground(this.color2Option.getSelected());
		if (names[this.options.getSelectedIndex()].equalsIgnoreCase("left")) {
			this.s.removeAll();
			if (this.mainController.getBackground() == Color.red) {
				this.s.setBorder(BorderFactory.createMatteBorder(1, 20, 1, 1,
						Color.BLACK));
			} else {
				this.s.setBorder(BorderFactory.createMatteBorder(1, 20, 1, 1,
						Color.RED));
			}
			this.s.repaint();
			op.repaint();
			this.repaint();
		} else {
			if (names[this.options.getSelectedIndex()]
					.equalsIgnoreCase("right")) {
				this.s.removeAll();
				if (this.mainController.getBackground() == Color.red) {
					this.s.setBorder(BorderFactory.createMatteBorder(1, 1, 1,
							20, Color.BLACK));
				} else {
					this.s.setBorder(BorderFactory.createMatteBorder(1, 1, 1,
							20, Color.RED));
				}
				this.s.repaint();
				op.repaint();
				this.repaint();
			}
		}
	}

	class theHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			colorChanged();
		}
	}

	public int getPanelPostion() {
		return this.options.getSelectedIndex() +1;
	}

}
