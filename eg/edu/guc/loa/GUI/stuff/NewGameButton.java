package eg.edu.guc.loa.GUI.stuff;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import eg.edu.guc.loa.GUI.mainpanel.MainPanel;

@SuppressWarnings("serial")
public class NewGameButton extends JButton {

	private MainPanel mainPanel;

	public NewGameButton(String text, MainPanel mainPanel) {
		this.setText(text);
		this.setFocusable(false);
		this.mainPanel = mainPanel;
	}

	public void mouseClicked() {
		int reply = JOptionPane.showConfirmDialog(null,
				"Are you sure, you want start new Game?!", "Warning Message!!",
				JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
			this.mainPanel.reset();
		}
	}

}
