package eg.edu.guc.loa.GUI.stuff;

import java.awt.Font;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class PlainJButton extends JButton {

	public PlainJButton() {
		this.setEnabled(false);
		this.setBorderPainted(false);
		this.setContentAreaFilled(false);
		this.setOpaque(false);
		this.setFont(new Font("Verdana", Font.ITALIC, 20));
	}

	public PlainJButton(String text) {
		this();
		this.setText(text);
	}
}
