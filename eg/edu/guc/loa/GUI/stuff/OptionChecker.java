package eg.edu.guc.loa.GUI.stuff;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JPanel;

import eg.edu.guc.loa.GUI.MainController;

@SuppressWarnings("serial")
public class OptionChecker extends JPanel{
	private MainController  mainController;
	private CheckerButton draw;
	
	public OptionChecker(MainController mainController,String name) {
		if(this.getHeight() >this.getWidth()){
			this.setSize(this.getHeight() , this.getHeight() );
		}
		if(this.getHeight() <this.getWidth()){
			this.setSize(this.getWidth() , this.getWidth() );
		}
		this.mainController =mainController;
		this.setOpaque(false);
		this.setName(name);
		this.setLayout(new BorderLayout());
		this.draw = new CheckerButton(this.getBackground().darker(),this.mainController.getBoard().getPlayerTurn());
		draw.setFont(new Font("Verdana", Font.BOLD, 14));
		draw.setToolTipText(this.mainController.getBoard().getPlayerTurn().getName() + " "+name);
		draw.setForeground(this.mainController.getBoard().getPlayerTurn().getColor());
		this.draw.setOpaque(false);
		this.add(draw,BorderLayout.CENTER);
		this.setEnabled(false);
	}

	public void changeTurn() {
		this.draw.setPlayerButton(this.mainController.getBoard().getPlayerTurn());
		draw.setToolTipText(this.mainController.getBoard().getPlayerTurn().getName() + " Turn");
		draw.setForeground(this.mainController.getBoard().getPlayerTurn().getColor());
	}

}
