package eg.edu.guc.loa.GUI.stuff;

import java.awt.Font;

import javax.swing.JButton;

import eg.edu.guc.loa.engine.Player;

@SuppressWarnings("serial")
public class ScoreTextField extends JButton {
	private Player player;

	public ScoreTextField(Player p) {
		this.setName(p.getName());
		this.player = p;
		this.setText(p.getName() + " : " + p.getScore());
		this.setToolTipText(p.getName() + "'s Score");
		this.setBorderPainted(false);
		this.setFocusable(false);
		this.setOpaque(false);
		this.setContentAreaFilled(false);
		this.setForeground(player.getColor());
		this.setFont(new Font("ss",Font.ITALIC,16));
	}

	public void renew() {
		this.setName(player.getName());
		this.setForeground(player.getColor());
		this.setText(player.getName() + ": " + player.getScore());
		this.repaint();
	}

	public void setPlayer(Player Player) {
		this.player = Player;
		this.repaint();
	}
}
