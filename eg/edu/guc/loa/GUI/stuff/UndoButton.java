package eg.edu.guc.loa.GUI.stuff;

import javax.swing.JButton;

import eg.edu.guc.loa.GUI.MainController;
import eg.edu.guc.loa.GUI.mainpanel.CheckersPanel;
import eg.edu.guc.loa.engine.Board;
import eg.edu.guc.loa.engine.stuff.LinkList;

@SuppressWarnings("serial")
public class UndoButton extends JButton {
	private static CheckersPanel checkersPanel;
	private static final Board DEFAULT_BOARD = new Board();
	private LinkList boards;

	public UndoButton(String n) {
		this.setText(n);
		this.setEnabled(false);
		this.setFocusable(false);
		this.setBorderPainted(false);
		this.setOpaque(true);
	}

	public UndoButton(String n, Board b) {
		this.setName(n);
		this.setText(n);
		DEFAULT_BOARD.setEqual(b);
		boards = new LinkList();
		this.setVisible(false);
		this.setFocusable(false);
		this.setBorderPainted(false);
		this.setOpaque(true);
	}

	public void saveCurrentBoard(Board b) {
		Board copy = new Board();
		copy.setEqual(b);
		boards.insertLast(copy);
		if (boards.size() > 1) {
			this.setVisible(true);
		}
	}

	public void mouseClicked() {
		if (boards.get(boards.size()).equal(DEFAULT_BOARD)) {
			checkersPanel.getEditor().setText("no undo");
			this.setVisible(false);
			return;
		} else {
			checkersPanel.loadBoard(this.boards.get(this.boards.size() - 1));
			this.boards.remove(this.boards.get(this.boards.size()));
			if (this.boards.size() == 1) {
				this.setVisible(false);
			}
		}
	}

	public void setChekersPanel(CheckersPanel p) {
		checkersPanel = p;
	}

	public void reset(Board b) {
		boards = new LinkList();
		this.saveCurrentBoard(b);
		this.setVisible(false);
	}

	public void applyChanges(MainController mainController) {
		if (boards.size() != 0) {
			for(int i =1 ; i < boards.size()+1;i++) {
				boards.get(i).getFirstPlayer().setName(mainController.getBoard().getFirstPlayer().getName());
				boards.get(i).getSecondPlayer().setName(mainController.getBoard().getSecondPlayer().getName());
				boards.get(i).getFirstPlayer().setColor(mainController.getBoard().getFirstPlayer().getColor());
				boards.get(i).getSecondPlayer().setColor(mainController.getBoard().getSecondPlayer().getColor());
			}
		}
	}

}
