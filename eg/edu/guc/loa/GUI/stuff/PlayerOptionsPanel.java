package eg.edu.guc.loa.GUI.stuff;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eg.edu.guc.loa.engine.Player;

@SuppressWarnings("serial")
public class PlayerOptionsPanel extends JPanel {
	private Player p;
	private JTextField name;
	private ColorList colorList;
	private CheckerButton checkerButton;
	private int x;

	public PlayerOptionsPanel(Player player,int x) {
		p = new Player("",Color.black);
		this.p.setEqual(player);
		this.setOpaque(false);
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.x =x;
		addName();
		addChecker();
		addColorList();
		this.add(new PlainJButton(" "));
		this.add(new PlainJButton(" "));
	}

	private void addChecker() {
		checkerButton = new CheckerButton(Color.black, p);
		checkerButton.setToolTipText("Checker Color Example");
		this.checkerButton.setOpaque(false);
		JPanel temp = new JPanel(new BorderLayout());
		temp.setOpaque(false);
		temp.add(new PlainJButton("  "), BorderLayout.EAST);
		temp.add(new PlainJButton("  "), BorderLayout.WEST);
		JPanel UpperHorizontalPanel = new JPanel(new GridLayout(0, 1));
		UpperHorizontalPanel.setOpaque(false);
		//UpperHorizontalPanel.add(new PlainJButton("  "));
		UpperHorizontalPanel.add(new PlainJButton("  "));
		temp.add(UpperHorizontalPanel, BorderLayout.NORTH);
		JPanel DownHorizontalPanel = new JPanel(new GridLayout(0, 1));
		DownHorizontalPanel.setOpaque(false);
		DownHorizontalPanel.add(new PlainJButton("  "));
		temp.add(DownHorizontalPanel, BorderLayout.SOUTH);
		temp.add(checkerButton);
		this.add(temp);
	}

	private void addColorList() {
		colorList = new ColorList(p.getColor());
		colorList.setSelected(p.getColor());
		colorList.addActionListener(new theHandler());
		JPanel temp = new JPanel(new GridLayout(0, 1));
		temp.setOpaque(false);
		temp.add(new PlainJButton("  "));
		temp.add(colorList);
		this.add(temp);
	}

	private void addName() {
		name = new JTextField(p.getName());
		name.setFont(new Font("Verdana", Font.ITALIC, 20));
		name.setForeground(p.getColor());
		name.setBackground(this.getBackground());
		name.setSize(this.getWidth(), this.getHeight());
		name.setAutoscrolls(false);
		name.setOpaque(false);
		JPanel temp = new JPanel(new BorderLayout());
		temp.setOpaque(false);
		JPanel temp1 = new JPanel(new GridLayout(0, 1));
		temp1.setOpaque(false);
		temp1.add(new PlainJButton("  "));
		temp1.add(new PlainJButton("  "));
		temp.add(temp1, BorderLayout.NORTH);
		JPanel temp2 = new JPanel(new GridLayout(0, 1));
		temp2.setOpaque(false);
		temp2.add(new PlainJButton("  "));
		temp.add(temp2, BorderLayout.SOUTH);
		name.setBounds(20, 20, 20, 20);
		temp.add(name);
		if(x==1){
			name.setToolTipText("1st Player Name");
		} else {
			if(x ==2){
				name.setToolTipText("2nd Player Name");
			}
		}
		this.add(temp);
	}

	private void colorChanged() {
		p.setColor(colorList.getSelected());
		name.setForeground(p.getColor());
		checkerButton.setPlayerButton(p);
		checkerButton.repaint();
		this.repaint();
	}
	
	public String getNewName(){
		return name.getText();
	}
	
	public Color getNewColor(){
		return colorList.getSelected();
	}

	class theHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			colorChanged();
		}
	}

	public void reset(Player newP) {
		p.setEqual(newP);
		colorList.setSelected(p.getColor());
		name.setText(p.getName());
		this.checkerButton.setPlayerButton(p);
		this.repaint();
	}

}
