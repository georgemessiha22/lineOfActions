package eg.edu.guc.loa.GUI.stuff;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;

import javax.swing.JPanel;

import eg.edu.guc.loa.GUI.Handler;
import eg.edu.guc.loa.engine.Player;

@SuppressWarnings("serial")
public class CheckerButton extends JPanel {
	private Color DEFAULT_COLOR;
	private Color currentColor;
	private Color possibleMoveColor = Color.gray.darker().darker().darker()
			.darker();
	private Color selected = Color.green.darker().darker().darker().darker();
	private boolean isSelected = false;
	private Player player;
	private MouseListener e;
	private boolean flag;

	
	public Color getSelected() {
		return selected;
	}
	public void setDEFAULT_COLOR(Color dEFAULT_COLOR) {
		DEFAULT_COLOR = dEFAULT_COLOR;
		this.repaint();
	}

	public void setPossibleMoveColor(Color possibleMoveColor) {
		this.possibleMoveColor = possibleMoveColor;
	}

	public void setSelected(Color selected) {
		this.selected = selected;
	}

	public Player getPlayer() {
		return player;
	}

	public CheckerButton(Color defaultColor, Player p) {
		this.DEFAULT_COLOR = defaultColor;
		if (p != null) {
			currentColor = p.getColor();
			player = p;
		} else {
			currentColor = DEFAULT_COLOR;
		}
		this.setEnabled(false);
		this.e = null;
		this.setBackground(this.DEFAULT_COLOR);
		this.setFocusable(false);
		this.repaint();
	}

	public CheckerButton(Color defaultColor) {
		this.DEFAULT_COLOR = defaultColor;
		currentColor = DEFAULT_COLOR;
		this.setEnabled(false);
		this.e = null;
		this.setBackground(this.DEFAULT_COLOR);
		this.setFocusable(false);
		this.repaint();
	}

	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		super.paintComponent(g2);
		Ellipse2D tire1;
			tire1 = new Ellipse2D.Double(5, 5, this.getWidth()-10 ,this.getHeight() -10);
		if (isSelected) {
			g2.setColor(selected);
		} else {
			if (flag) {
				g2.setColor(possibleMoveColor);
			} else {
				g2.setColor(currentColor);
			}
		} 
		g2.fill(tire1);
	}

	public void setEmptyButton() {
		currentColor = DEFAULT_COLOR;
		this.setBackground(DEFAULT_COLOR);
		this.player = null;
		repaint();
	}

	// set the the button to the player button
	public void setPlayerButton(Player p) {
		if (p != null) {
			currentColor = p.getColor();
			this.player = p;
			this.repaint();
		} else {
			this.setEmptyButton();
		}
		this.setBackground(DEFAULT_COLOR);
	}

	public void selected() {
		this.setBackground(selected);
	}

	public void unSelect() {
		this.setBackground(this.DEFAULT_COLOR);
	}

	public boolean isFull() {
		if (this.currentColor.equals(DEFAULT_COLOR))
			return false;
		else
			return true;
	}

	public void changePossible() {
		flag = true;
		this.repaint();
	}

	public void previousState() {
		flag = false;
		this.repaint();
	}

	public void setMouseListener(MouseListener e) {
		this.e = e;
		this.addMouseListener(this.e);
	}

	public void blockButton() {
		isSelected = false;
		if (this.e != null) {
			this.removeMouseListener(this.e);
		}
	}

	public void unBlockButton() {
		isSelected = false;
		this.repaint();
		this.setMouseListener(this.e);
	}

	public void addMouseActionListener(Handler h) {
		this.addMouseListener(h);
	}

	public void unClickedPossible() {
		isSelected = false;
		this.repaint();
	}

	public void clickedPossible() {
		isSelected = true;
		this.repaint();
	}

	public void notSelected() {
		isSelected = false;
	}
	
}
