package eg.edu.guc.loa.GUI.stuff;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class ColorList extends JPanel {
	private JComboBox<String> thisx;
	private JScrollPane sp;
	private static String[] colornames = {"Default/Previous", "White", "Black", "Blue", "Red",
			"Cyan", "Bright Gray", "Gray", "Dark Gray", "Green", "Pink","Magneta",
			"Orange", "Yellow" };
	private Color[] colors = {null,Color.WHITE, Color.BLACK, Color.BLUE,
			Color.RED, Color.CYAN, Color.LIGHT_GRAY, Color.GRAY,
			Color.DARK_GRAY, Color.GREEN, Color.PINK,Color.MAGENTA, Color.ORANGE,
			Color.YELLOW };

	public ColorList(Color DefaultColor) {
		this.setOpaque(false);
		colors[0] = DefaultColor;
		thisx = new JComboBox<String>(colornames);
		thisx.scrollRectToVisible(new Rectangle(20, 20));
		thisx.setSize(20, 20);
		sp = new JScrollPane(thisx);
		thisx.setToolTipText("Checker Color");
		sp.setToolTipText("Checker Color");
		sp.setOpaque(false);
		this.add(sp);
	}

	public void setSelected(Color c) {
		for (int i = 0; i < colors.length; i++) {
			if (c.equals(colors[i])) {
				thisx.setSelectedIndex(i);
			}
		}
		this.repaint();
	}

	public Color getSelected() {
		return thisx.getSelectedIndex() > -1 ? colors[thisx.getSelectedIndex()]
				: null;
	}

	public void addActionListener(ActionListener l) {
		thisx.addActionListener(l);
	}

	public Color getDefault() {
		return colors[0];
	}
	
	public int getSelectedIndex(){
		return thisx.getSelectedIndex();
	}

	public void setSelectedIndex(int i){
		thisx.setSelectedIndex(i);
	}
}
