package eg.edu.guc.loa.GUI.mainpanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTextField;

import eg.edu.guc.loa.GUI.MainController;
import eg.edu.guc.loa.GUI.stuff.CheckerButton;
import eg.edu.guc.loa.GUI.stuff.PlainJButton;
import eg.edu.guc.loa.engine.Board;
import eg.edu.guc.loa.engine.Point;

@SuppressWarnings("serial")
public class CheckersPanel extends JPanel {
	private CheckerButton[][] buttonBoard;
	private JPanel centerPanel;
	private JPanel[] positionsButtons;
	private JPanel checkers;
	private JTextField editor;
	private CheckerButton start;
	private MainController mainController;
	
	//colors
	private final Color defaultColor = new Color(139, 69, 19);
	private final Color defaultColor1 = new Color(139, 69, 19).darker()
			.darker();
	private final Color DefaultPossibleMoveColor = Color.gray.darker().darker()
			.darker().darker();
	private final Color DefaultSelected = Color.green.darker().darker()
			.darker().darker();
	private Color color1 = defaultColor;
	private Color color2 = defaultColor1;
	private Color possibleMoveColor = DefaultPossibleMoveColor;
	private Color selectedColor = this.DefaultSelected;
	

	public CheckersPanel(MainController mainController2) {
		this.setOpaque(false);
		this.mainController = mainController2;
		checkersPopulate();
		editorPopulate();
		positionsButtonPopulate();
		display();
		this.addMouseActionListener();
	}

	public void setController(MainPanel mainPanel) {
		mainPanel.getControllerPanel().saveCurrentBoard(
				this.mainController.getBoard());
		mainPanel.getControllerPanel().setChekersPanel(this);
	}

	public Board getBoard() {
		return this.mainController.getBoard();
	}

	public void loadBoard(Board oldBoard) {
		if (this.mainController.getBoard().isGameOver()) {
			undoGameOver();
		}
		if (this.start != null) {
			this.unColorPossible(start);
			this.start = null;
		}
		findDifferenceAndSwitch(oldBoard);
		this.mainController.getBoard().setEqual(oldBoard);
		this.mainController.getMainPanel().getControllerPanel()
				.getTurnChecker().changeTurn();
	}

	public void reset() {
		this.removeAll();
		checkersPopulate();
		editorPopulate();
		positionsButtonPopulate();
		display();
		this.addMouseActionListener();
		this.repaint();
		if (this.start != null) {
			this.start.unSelect();
			this.start = null;
		}
		this.undoGameOver();
	}

	public void addMouseActionListener() {
		for (int i = 0; i < buttonBoard.length; i++) {
			for (int j = 0; j < buttonBoard[i].length; j++) {
				buttonBoard[i][j].addMouseActionListener(this.mainController
						.getHandler());
			}
		}
	}

	public void setVisible() {
		this.setVisible(true);
	}

	public void setHidden() {
		this.setVisible(false);
	}

	public boolean findButton(MouseEvent e) {
		for (int i = 0; i < buttonBoard.length; i++) {
			for (int j = 0; j < buttonBoard[i].length; j++) {
				if (buttonBoard[j][i] == e.getSource()) {
					return true;
				}
			}
		}
		return false;
	}

	public void mouseEntered(MouseEvent e) {
		if (!mainController.getBoard().isGameOver()) {
			for (int i = 0; i < buttonBoard.length; i++) {
				for (int j = 0; j < buttonBoard[i].length; j++) {
					if (e.getSource() == buttonBoard[i][j]
							&& buttonBoard[i][j].isFull()) {
						ArrayList<Point> x = mainController.getBoard()
								.getPossibleMoves(new Point(j, i));
						if (x.size() != 0) {
							for (int k = 0; k < x.size(); k++) {
								buttonBoard[x.get(k).getY()][x.get(k).getX()]
										.changePossible();
							}
							editor.setText("Possible Moves.");
						} else {
							if (buttonBoard[i][j].getPlayer().equal(
									mainController.getBoard().getPlayerTurn()))
								editor.setText("No Possible Moves.");
						}
					}
				}
			}
		}
	}

	public void mouseExited(MouseEvent e) {
		if (!mainController.getBoard().isGameOver()) {
			editor.setText("");
			for (int i = 0; i < buttonBoard.length; i++) {
				for (int j = 0; j < buttonBoard[i].length; j++) {
					if (e.getSource() == buttonBoard[i][j]
							&& buttonBoard[i][j].isFull()) {
						ArrayList<Point> x = mainController.getBoard()
								.getPossibleMoves(new Point(j, i));
						for (int k = 0; k < x.size(); k++) {
							buttonBoard[x.get(k).getY()][x.get(k).getX()]
									.previousState();
						}
					}
				}
			}
		}
	}

	public void mouseClicked(MouseEvent e) {
		CheckerButton end = getButton(e);
		if (!mainController.getBoard().isGameOver()) {
			if (end.getPlayer() != null
					&& end.getPlayer().equal(
							mainController.getBoard().getPlayerTurn())) {
				if (this.start != null) {
					this.unColorPossible(start);
				}
				this.start = end;
				this.start.selected();
				this.colorPossible(e);
				return;
			}
			if (start != null && start.getPlayer() != null) {
				if (start.getPlayer().equal(
						mainController.getBoard().getPlayerTurn())) {
					if (isPossibleMove(findButtonPosition(end))) {
						this.unColorPossible(start);
						if (mainController.getBoard().move(
								findButtonPosition(this.start),
								findButtonPosition(end))) {
							if (!mainController.getBoard().isGameOver()) {
								this.mainController.getMainPanel()
										.getControllerPanel().getTurnChecker()
										.changeTurn();
							}

							this.moveButton(e);
						} else {
							editor.setText("Invalid move.");
							this.start = null;
						}
					} else {
						editor.setText("Invalid move.");
						this.unColorPossible(start);
						this.start = null;
					}
				} else {
					if (mainController
							.getBoard()
							.getPlayerTurn()
							.getName()
							.equals(mainController.getBoard().getFirstPlayer()
									.getName())) {
						editor.setText("It's "
								+ mainController.getBoard().getFirstPlayer()
										.getName() + " turn.");
					} else {
						editor.setText("It's "
								+ mainController.getBoard().getSecondPlayer()
										.getName() + " turn.");
					}
					this.unColorPossible(start);
					this.start = null;
				}
			} else {
				if (start == null
						&& end.isFull()
						&& end.getPlayer().equal(
								mainController.getBoard().getPlayerTurn())) {
					this.start = end;
					this.start.selected();
				}
			}
		} else {
			if (start != null) {
				this.unColorPossible(start);
			}
			gameOver();
		}
	}

	public JTextField getEditor() {
		return editor;
	}

	private void checkersPopulate() {
		checkers = new JPanel(new GridLayout(mainController.getBoard()
				.getBoard().length,
				mainController.getBoard().getBoard()[0].length));
		checkers.setOpaque(false);
		buttonBoard = new CheckerButton[mainController.getBoard().getBoard().length][mainController
				.getBoard().getBoard()[0].length];
		for (int i = 0; i < buttonBoard.length; i++) {
			for (int j = 0; j < buttonBoard[i].length; j++) {
				if ((i % 2 == 0 && j % 2 != 0) || (i % 2 != 0 && j % 2 == 0)) {

					buttonBoard[i][j] = new CheckerButton(this.color2,
							mainController.getBoard().getBoard()[i][j]);

					checkers.add(buttonBoard[i][j]);
				} else {
					buttonBoard[i][j] = new CheckerButton(color1,
							mainController.getBoard().getBoard()[i][j]);
					checkers.add(buttonBoard[i][j]);
				}
			}
		}
	}

	private void findDifferenceAndSwitch(Board oldBoard) {
		Point p = new Point(-1, -1);
		Point n = new Point(-1, -1);
		for (int i = 0; i < oldBoard.getBoard().length
				&& (p.equal(new Point(-1, -1))); i++) {
			for (int j = 0; j < oldBoard.getBoard()[i].length
					&& (p.equal(new Point(-1, -1))); j++) {
				if (oldBoard.getBoard()[i][j] != null
						&& this.mainController.getBoard().getBoard()[i][j] != null) {
					if (!(oldBoard.getBoard()[i][j].equal(this.mainController
							.getBoard().getBoard()[i][j]))) {
						if (p.equal(new Point(-1, -1))) {
							p.equal(new Point(-1, -1));
						}
					}
				} else {
					if (oldBoard.getBoard()[i][j] != null
							&& this.mainController.getBoard().getBoard()[i][j] == null) {
						if (p.equal(new Point(-1, -1))) {
							p = new Point(j, i);
						}
					}
				}
			}
		}
		if (!(p.equal(new Point(-1, -1)))) {
			ArrayList<Point> possible = oldBoard.getPossibleMovesHelper(p);
			for (int i = 0; i < possible.size(); i++) {
				if (oldBoard.isEmptyPosition(possible.get(i))
						&& !(this.mainController.getBoard()
								.isEmptyPosition(possible.get(i)))) {
					n = possible.get(i);
					break;
				}
				if (!(this.mainController.getBoard().isEmptyPosition(possible
						.get(i)))
						&& !(oldBoard.isEmptyPosition(possible.get(i)))) {
					if (!(oldBoard.getBoard()[possible.get(i).getY()][possible
							.get(i).getX()]
							.equal(this.mainController.getBoard().getBoard()[possible
									.get(i).getY()][possible.get(i).getX()]))) {
						n = possible.get(i);
						break;
					}
				}
			}
			this.buttonBoard[p.getY()][p.getX()].setPlayerButton(oldBoard
					.getPositionPlayer(p));
			this.buttonBoard[n.getY()][n.getX()].setPlayerButton(oldBoard
					.getPositionPlayer(n));
			if (this.mainController.getBoard().getPositionPlayer(p) != null) {
				if (this.mainController.getBoard().getPositionPlayer(p)
						.equal(this.mainController.getBoard().getFirstPlayer())) {
					this.mainController.getBoard().getFirstPlayer()
							.moveChecker(n, p);
				}
				if (this.mainController
						.getBoard()
						.getPositionPlayer(p)
						.equal(this.mainController.getBoard().getSecondPlayer())) {
					this.mainController.getBoard().getSecondPlayer()
							.moveChecker(n, p);
				}
			}
		}
		this.mainController.getMainPanel().getControllerPanel()
				.getTurnChecker().changeTurn();
	}

	private void editorPopulate() {
		editor = new JTextField();
		editor.setEditable(false);
		editor.setFont(new Font("Verdana", Font.ITALIC, 16));
		editor.setForeground(Color.BLACK);
		editor.setBackground(Color.white);
		editor.setToolTipText("Instructions. :)");
	}

	private void positionsButtonPopulate() {
		this.positionsButtons = new JPanel[2];
		this.positionsButtons[0] = new JPanel(new BorderLayout());
		this.positionsButtons[0].setOpaque(false);
		this.positionsButtons[1] = new JPanel(new GridLayout(
				this.buttonBoard.length, 0));
		this.positionsButtons[1].setOpaque(false);
		JPanel temp = new JPanel(new GridLayout(0, this.buttonBoard[0].length));
		temp.setOpaque(false);
		for (int i = 0; i < this.positionsButtons.length; i++) {
			for (int j = 0; j < this.buttonBoard.length; j++) {
				if (i == 0) {
					if (j == 0) {
						this.positionsButtons[i].add(new PlainJButton("Y\\X"),
								BorderLayout.WEST);
					}
					temp.add(new PlainJButton(j + ""));
					this.positionsButtons[i].add(temp, BorderLayout.CENTER);
				} else {
					PlainJButton n = new PlainJButton();
					switch (j) {
					case 0:
						n.setText("A" + j);
						break;
					case 1:
						n.setText("B" + j);
						break;
					case 2:
						n.setText("C" + j);
						break;
					case 3:
						n.setText("D" + j);
						break;
					case 4:
						n.setText("E" + j);
						break;
					case 5:
						n.setText("F" + j);
						break;
					case 6:
						n.setText("G" + j);
						break;
					case 7:
						n.setText("H" + j);
						break;
					case 8:
						n.setText("I" + j);
						break;
					case 9:
						n.setText("J" + j);
						break;
					case 10:
						n.setText("K" + j);
						break;
					case 11:
						n.setText("L" + j);
						break;
					case 12:
						n.setText("M" + j);
						break;
					case 13:
						n.setText("N" + j);
						break;
					case 14:
						n.setText("O" + j);
						break;
					case 15:
						n.setText("P" + j);
						break;
					case 16:
						n.setText("Q" + j);
						break;
					default:
						n.setText("byhrag");
					}
					this.positionsButtons[i].add(n);
				}
			}
		}
	}

	private void display() {
		this.setLayout(new BorderLayout());
		centerPanel = new JPanel();
		centerPanel.setOpaque(false);
		this.centerPanel.setLayout(new BorderLayout());
		this.add(this.positionsButtons[0], BorderLayout.NORTH);
		this.centerPanel.add(checkers, BorderLayout.CENTER);
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(this.positionsButtons[1], BorderLayout.WEST);
		this.add(editor, BorderLayout.SOUTH);
	}

	private String getWinner() {
		if (mainController.getBoard().getWinner() == 1) {
			if (this.mainController.getBoard().getWinnerPlayer().getColor() == Color.BLACK) {
				editor.setForeground(Color.RED);
			}
			editor.setBackground(this.mainController.getBoard()
					.getWinnerPlayer().getColor());
			return mainController.getBoard().getFirstPlayer().getName()
					+ " Won!";
		}
		if (mainController.getBoard().getWinner() == 2) {
			if (this.mainController.getBoard().getWinnerPlayer().getColor() == Color.BLACK) {
				editor.setForeground(Color.RED);
			}
			editor.setBackground(this.mainController.getBoard()
					.getWinnerPlayer().getColor());
			return mainController.getBoard().getSecondPlayer().getName()
					+ " Won!";
		}
		return "Game Over! No Winner. ";
	}

	private void moveButton(MouseEvent e) {
		if (!(mainController.getBoard().getPlayerTurn().getName()
				.equals(mainController.getBoard().getFirstPlayer().getName()))) {
			getButton(e).setPlayerButton(
					mainController.getBoard().getFirstPlayer());
		} else {
			getButton(e).setPlayerButton(
					mainController.getBoard().getSecondPlayer());
		}
		this.mainController.getMainPanel().getControllerPanel()
				.saveCurrentBoard(this.mainController.getBoard());
		this.start.setEmptyButton();
		this.start = null;
		if (mainController.getBoard().isGameOver()) {
			gameOver();
		}
	}

	boolean isPossibleMove(Point end) {
		Point start = findButtonPosition(this.start);
		ArrayList<Point> startPossibleMoves = mainController.getBoard()
				.getPossibleMoves(start);
		if (startPossibleMoves.isEmpty()) {
			if (mainController
					.getBoard()
					.getPlayerTurn()
					.getName()
					.equals(mainController.getBoard().getFirstPlayer()
							.getName())) {
				editor.setText("It's "
						+ mainController.getBoard().getFirstPlayer().getName()
						+ " turn.");
			} else {
				editor.setText("It's "
						+ mainController.getBoard().getSecondPlayer().getName()
						+ " turn.");
			}
		} else {
			for (int i = 0; i < startPossibleMoves.size(); i++) {
				if (end.compareTo(startPossibleMoves.get(i)) == 0) {
					return true;
				}
			}
		}
		return false;
	}

	private CheckerButton getButton(MouseEvent e) {
		for (int i = 0; i < buttonBoard.length; i++) {
			for (int j = 0; j < buttonBoard[i].length; j++) {
				if (buttonBoard[j][i] == e.getSource()) {
					return buttonBoard[j][i];
				}
			}
		}
		return null;
	}

	private Point findButtonPosition(CheckerButton button) {
		for (int i = 0; i < buttonBoard.length; i++) {
			for (int j = 0; j < buttonBoard[i].length; j++) {
				if (buttonBoard[j][i] == button) {
					return new Point(i, j);
				}
			}
		}
		return null;
	}

	private void unColorPossible(CheckerButton button) {
		if (button != null) {
			button.unSelect();
		}
		if (!mainController.getBoard().isGameOver()) {
			for (int i = 0; i < buttonBoard.length; i++) {
				for (int j = 0; j < buttonBoard[i].length; j++) {
					if (button == buttonBoard[i][j]
							&& buttonBoard[i][j].isFull()) {
						ArrayList<Point> x = mainController.getBoard()
								.getPossibleMovesHelper(new Point(j, i));
						if (x.size() != 0) {
							for (int k = 0; k < x.size(); k++) {
								buttonBoard[x.get(k).getY()][x.get(k).getX()]
										.unClickedPossible();
							}
						}
					}
				}
			}
		}
	}

	private void colorPossible(MouseEvent e) {
		if (!mainController.getBoard().isGameOver()) {
			for (int i = 0; i < buttonBoard.length; i++) {
				for (int j = 0; j < buttonBoard[i].length; j++) {
					if (e.getSource() == buttonBoard[i][j]
							&& buttonBoard[i][j].isFull()) {
						ArrayList<Point> x = mainController.getBoard()
								.getPossibleMoves(new Point(j, i));
						if (x.size() != 0) {
							for (int k = 0; k < x.size(); k++) {
								buttonBoard[x.get(k).getY()][x.get(k).getX()]
										.clickedPossible();
							}
						}
					}
				}
			}
		}
	}

	private void gameOver() {
		for (int i = 0; i < this.buttonBoard.length; i++) {
			for (int j = 0; j < this.buttonBoard[i].length; j++) {
				this.buttonBoard[i][j].blockButton();
			}
		}
		editor.setText(getWinner());
		this.mainController.setEndPageVisible();
	}

	private void undoGameOver() {
		for (int i = 0; i < this.buttonBoard.length; i++) {
			for (int j = 0; j < this.buttonBoard[i].length; j++) {
				this.buttonBoard[i][j].unBlockButton();
			}
		}
		editor.setText("");
		editor.setForeground(Color.BLACK);
		editor.setBackground(Color.WHITE);
	}

	public Color getDefaultColor() {
		return defaultColor;
	}

	public Color getSelectedColor() {
		return selectedColor;
	}

	public void setSelectedColor(Color selectedColor) {
		this.selectedColor = selectedColor;
	}

	public void setColor1(Color defaultColor) {
		this.color1 = defaultColor;
	}

	public Color getDefaultColor1() {
		return defaultColor1;
	}

	public void setColor2(Color color2) {
		this.color2 = color2;
	}

	public void setPossibleMoveColor(Color possibleMoveColor) {
		this.possibleMoveColor = possibleMoveColor;
	}

	public Color getDefaultPossibleMoveColor() {
		return DefaultPossibleMoveColor;
	}

	public Color getColor1() {
		return color1;
	}

	public Color getColor2() {
		return color2;
	}

	public Color getPossibleMoveColor() {
		return possibleMoveColor;
	}

	public Color getDefaultSelected() {
		return DefaultSelected;
	}

	public void applyChanges() {
		for (int i = 0; i < this.buttonBoard.length; i++) {
			for (int j = 0; j < this.buttonBoard[i].length; j++) {
				buttonBoard[i][j].setPossibleMoveColor(possibleMoveColor);
				buttonBoard[i][j].setSelected(selectedColor);
				if ((i % 2 == 0 && j % 2 != 0) || (i % 2 != 0 && j % 2 == 0)) {
					buttonBoard[i][j].setDEFAULT_COLOR(this.color2);
				} else {
					buttonBoard[i][j].setDEFAULT_COLOR(this.color1);
				}
				if (this.mainController.getBoard().getBoard()[i][j] != null) {
					buttonBoard[i][j].setPlayerButton(this.mainController
							.getBoard().getBoard()[i][j]);
				} else {
					buttonBoard[i][j].setEmptyButton();
				}
			}
		}
		this.repaint();
	}

}
