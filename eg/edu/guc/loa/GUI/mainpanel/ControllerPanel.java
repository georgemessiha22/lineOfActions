package eg.edu.guc.loa.GUI.mainpanel;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import eg.edu.guc.loa.GUI.MainController;
import eg.edu.guc.loa.GUI.stuff.OptionChecker;
import eg.edu.guc.loa.GUI.stuff.PlainJButton;
import eg.edu.guc.loa.GUI.stuff.ScoreTextField;
import eg.edu.guc.loa.GUI.stuff.UndoButton;
import eg.edu.guc.loa.engine.Board;

@SuppressWarnings("serial")
public class ControllerPanel extends JPanel {

	private ArrayList<JButton> buttons;
	private JPanel buttonsContainer;
	private CheckersPanel chekersPanel;
	private MainController  mainController;
	private OptionChecker turnChecker;
	private ArrayList<JButton> ScoreFileds;

	public ControllerPanel(MainPanel mainPanel,MainController mainController) {
		this.setLayout(new GridLayout(0,1));
		this.setOpaque(false);
		buttons = new ArrayList<JButton>();
		buttonsContainer = new JPanel (new GridLayout(0,1));
		buttonsContainer.setOpaque(false);
		buttonsContainer.add(new PlainJButton("  "));
		buttonsContainer.add(new PlainJButton("  "));
		this.add(buttonsContainer);
		this.mainController = mainController;
		buttonPopulate(this.mainController.getBoard(),mainPanel);
		this.addMouseActionListener();
	}

	private void buttonPopulate(Board b,MainPanel mainPanel) {
		UndoButtonPopulate(b);
		PlainButtonsPopulate();
		turnButtonPopulate();
		scoreTextFiled();
		addButtons();
	}
	
	public void scoreFieldRenew(){
		((ScoreTextField)this.ScoreFileds.get(1)).renew();
		((ScoreTextField)this.ScoreFileds.get(3)).renew();
	}
	
	public ScoreTextField getFirstPlayerTextField() {
		return ((ScoreTextField)this.ScoreFileds.get(1));
	}
	
	public ScoreTextField getSecondPlayerTextField() {
		return ((ScoreTextField)this.ScoreFileds.get(3));
	}
	
	private void scoreTextFiled() {
		this.ScoreFileds = new ArrayList<JButton>();
		ScoreTextField  firstPlayerScoreFiled = new ScoreTextField(this.mainController.getBoard().getFirstPlayer());
		ScoreTextField  secondPlayerScoreFiled =new ScoreTextField(this.mainController.getBoard().getSecondPlayer());
		this.ScoreFileds.add(new PlainJButton(" "));
		this.ScoreFileds.add(firstPlayerScoreFiled);
		this.ScoreFileds.add(new PlainJButton(" "));
		this.ScoreFileds.add(secondPlayerScoreFiled);
		for(int i = 0;i < 1; i++) {
		this.ScoreFileds.add(new PlainJButton("   "));
		}
		JPanel temp = new JPanel();
		temp.setOpaque(false);
		temp.setLayout(new GridLayout(0,1));
		for(int i =0 ; i <this.ScoreFileds.size();i++){
			temp.add(this.ScoreFileds.get(i));
		}
		this.add(temp);
	}

	private void turnButtonPopulate() {
		turnChecker = new OptionChecker(this.mainController,"Turn") ;
		this.add(turnChecker);
	}

	public OptionChecker getTurnChecker(){
		return turnChecker;
	}
	
	private void PlainButtonsPopulate() {
		for (int i =0 ; i< this.mainController.getBoard().getBoard().length-4;i++){
			this.add(new PlainJButton("   "));
		}
	}

	public UndoButton getUndoButton(){
		return ((UndoButton) this.getButton("Undo"));
	}
	
	private void addButtons() {
		for (int i = 0; i < this.buttons.size(); i++) {
			this.buttonsContainer.add(this.buttons.get(i));
		}
	}

	private void UndoButtonPopulate(Board b) {
		UndoButton undo = new UndoButton("Undo", b);
		undo.setName("Undo");
		this.buttons.add(undo);
	}

	public void saveCurrentBoard(Board b) {
		((UndoButton) getButton("Undo")).saveCurrentBoard(b);
	}

	public void addMouseActionListener() {
		for (int i = 0; i < this.buttons.size(); i++) {
			buttons.get(i).addMouseListener(this.mainController.getHandler());
		}
	}

	public void setChekersPanel(CheckersPanel chekersPanel) {
		this.chekersPanel = chekersPanel;
		((UndoButton) getButton("Undo")).setChekersPanel(this.chekersPanel);
	}

	private JButton getButton(String name) {
		for (int i = 0; i < this.buttons.size(); i++) {
			if (this.buttons.get(i).getName() != null &&this.buttons.get(i).getName().equalsIgnoreCase(name)) {
				return this.buttons.get(i);
			}
		}
		return null;
	}

	public void mouseClicked(MouseEvent e){
		JButton b = getButton(e);
		if(b.getName() == null) {
			return;
		}
		if (b.getName().equals("Undo")) {
			((UndoButton) this.getButton("Undo")).mouseClicked();
		return;
		}
	}
	
	private JButton getButton(MouseEvent e) {
		for (int i = 0; i < this.buttons.size(); i++) {
			if (this.buttons.get(i) == e.getSource()) {
				return this.buttons.get(i);
			}
		}
		return null;
	}
	
	public Boolean findButton(MouseEvent e) {
		for (int i = 0; i < this.buttons.size(); i++) {
			if (this.buttons.get(i) == e.getSource()) {
				return true;
			}
		}
		return false;
	}
	
	public void applyChanges() {
		this.turnChecker.changeTurn();
this.mainController.getMainPanel().getControllerPanel()
.getSecondPlayerTextField().renew();
this.mainController.getMainPanel().getControllerPanel()
.getFirstPlayerTextField().renew();
this.getUndoButton().applyChanges(this.mainController);
this.repaint();
	}

}
