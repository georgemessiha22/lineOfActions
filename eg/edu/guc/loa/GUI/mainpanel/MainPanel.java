package eg.edu.guc.loa.GUI.mainpanel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import eg.edu.guc.loa.GUI.Handler;
import eg.edu.guc.loa.GUI.MainController;
import eg.edu.guc.loa.GUI.stuff.NewGameButton;
import eg.edu.guc.loa.GUI.stuff.PlainJButton;
import eg.edu.guc.loa.engine.Board;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {
	private MainController mainController;
	private CheckersPanel checkersPanel;
	private ControllerPanel controllerPanel;
	private JPanel holder;
	private ArrayList<JButton> buttons;
	private int controllerPositison = 1;

	public MainPanel(MainController mainController) {
		this.mainController = mainController;
		this.setLayout(new BorderLayout());
		this.setOpaque(false);
		this.holder = new JPanel(new FlowLayout());
		this.holder.setOpaque(false);
		buttons = new ArrayList<JButton>();
		startNewButtonPopulate();
		OptionsButtonPopulate();
		HelpButtonPopulate();
		controllerPanelPopulate();
		checkersPanelPopulate();
		this.add(holder, BorderLayout.NORTH);
		addMouseActionListener();
	}

	private void startNewButtonPopulate() {
		NewGameButton n = new NewGameButton("New Game", this);
		n.setName("New Game");
		buttons.add(n);
		this.holder.add(n);
	}

	private void HelpButtonPopulate() {
		JButton n = new JButton("Help");
		n.setName("Help");
		buttons.add(n);
		this.holder.add(n);
		for (int i = 0; i < mainController.getBoard().getBoard().length * 2; i++) {
			this.holder.add(new PlainJButton("       "));
		}
	}

	private void OptionsButtonPopulate() {
		JButton options = new JButton("Options");
		options.setName("Options");
		options.setFocusable(false);
		buttons.add(options);
		this.holder.add(options);

	}

	public CheckersPanel getCheckersPanel() {
		return checkersPanel;
	}

	public void setCheckersPanel(CheckersPanel checkersPanel) {
		this.checkersPanel = checkersPanel;
	}

	public Board getBoard() {
		return this.mainController.getBoard();
	}

	public NewGameButton getNewGameButton() {
		return ((NewGameButton) this.getButton("New Game"));
	}

	public void mouseClicked(MouseEvent e) {
		JButton b = getButton(e);
		if (b.getName() == null) {
			return;
		}
		if (b.getName().equals("New Game")) {
			((NewGameButton) this.getButton("New Game")).mouseClicked();
			return;
		}
		if (b.getName().equals("Options")) {
			this.mainController.setOptionsPanelVisible();
		}
		if (b.getName().equals("Help")) {
			this.checkersPanel.setVisible(false);
			String s = "Rules:\n1.Checkers move in the 3 lines of action: horizontally, vertically, or diagonally.\n 2.The number of squares moved by a checker must be exaclty the same \nas the number of checkers(belonging to both players) in that line of action.\n 3.checker belonging to a player may jump over other checkers belonging to that \nplayer, but it may not jump over checkers belonging to the opponent. \n 4.A checker captures an opponent's checker by landing on it.\n \n\n Enjoy the Game.\n Credits:\n    Geaorge Samir\n    Mariam Botros\n    Shery Nazih";
			JOptionPane.showMessageDialog(this.checkersPanel, s, "Instructions",
					getPosition());
			this.checkersPanel.setVisible(true);
			this.add(checkersPanel, BorderLayout.CENTER);

			this.repaint();
		}
	}

	private JButton getButton(MouseEvent e) {
		for (int i = 0; i < this.buttons.size(); i++) {
			if (this.buttons.get(i) == e.getSource()) {
				return this.buttons.get(i);
			}
		}
		return null;
	}

	private JButton getButton(String name) {
		for (int i = 0; i < this.buttons.size(); i++) {
			if (this.buttons.get(i).getName() != null
					&& this.buttons.get(i).getName().equalsIgnoreCase(name)) {
				return this.buttons.get(i);
			}
		}
		return null;
	}

	public Boolean findButton(MouseEvent e) {
		for (int i = 0; i < this.buttons.size(); i++) {
			if (this.buttons.get(i) == e.getSource()) {
				return true;
			}
		}
		return false;
	}

	public void addMouseActionListener() {
		for (int i = 0; i < this.buttons.size(); i++) {
			buttons.get(i).addMouseListener(this.mainController.getHandler());
		}
	}

	public ControllerPanel getControllerPanel() {
		return controllerPanel;
	}

	private void controllerPanelPopulate() {
		controllerPanel = new ControllerPanel(this, this.mainController);
		this.add(controllerPanel, BorderLayout.WEST);
	}

	void checkersPanelPopulate() {
		checkersPanel = new CheckersPanel(this.mainController);
		checkersPanel.setController(this);
		this.add(checkersPanel, BorderLayout.CENTER);
	}

	public MouseListener getHandler() {
		return this.mainController.getHandler();
	}

	public void setHidden() {
		this.setVisible(false);
		this.removeMouseListener(this.mainController.getHandler());
	}

	public void setVisible(Handler handler) {
		this.setVisible(true);
		this.addMouseListener(handler);
	}

	public void reset() {
		if (this.mainController.getBoard().getWinnerPlayer() != null) {
			this.mainController.getBoard().getWinnerPlayer().increaseScore();
		}
		this.mainController.getBoard().reset();
		this.mainController.getMainPanel().getControllerPanel()
				.getFirstPlayerTextField()
				.setPlayer(this.mainController.getBoard().getFirstPlayer());
		this.mainController.getMainPanel().getControllerPanel()
				.getSecondPlayerTextField()
				.setPlayer(this.mainController.getBoard().getSecondPlayer());
		this.controllerPanel.scoreFieldRenew();
		this.checkersPanel.reset();
		this.controllerPanel.getUndoButton().reset(
				this.mainController.getBoard());
		this.controllerPanel.getTurnChecker().changeTurn();
	}

	public void applyChanges() {
		controllerPanel.applyChanges();
		this.checkersPanel.applyChanges();
		this.repaint();
	}

	public int getPosition() {
		return controllerPositison;
	}

	public void changePosition() {
		controllerPositison = 3 - controllerPositison;
		this.removeAll();
		if (controllerPositison == 2) {
			this.add(controllerPanel, BorderLayout.EAST);
		} else {
			this.add(controllerPanel, BorderLayout.WEST);
		}
		this.add(checkersPanel, BorderLayout.CENTER);

		this.add(holder, BorderLayout.NORTH);
	}
}
