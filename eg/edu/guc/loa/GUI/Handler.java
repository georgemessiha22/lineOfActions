package eg.edu.guc.loa.GUI;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Handler implements MouseListener {

	private MainController  mainController;

	public Handler(MainController mainController) {
		this.mainController = mainController;
	}

	public void mouseClicked(MouseEvent e) {
	if(mainController.getStartPage() != null) {
		if (mainController.getStartPage().findButton(e)) {
			mainController.getStartPage().mouseClicked(e);
			return;
		}}
		if(mainController.getMainPanel() != null) {
		if (mainController.getMainPanel().findButton(e)) {
			
			mainController.getMainPanel().mouseClicked(e);
			return;
		}
		if (mainController.getMainPanel().getCheckersPanel().findButton(e)) {
			mainController.getMainPanel().getCheckersPanel().mouseClicked(e);
			return;
		}
		if (mainController.getMainPanel().getControllerPanel().findButton(e)) {
			mainController.getMainPanel().getControllerPanel().mouseClicked(e);
			return;
		}
		}
		if(mainController.getOptionsPanel() != null){
		if (mainController.getOptionsPanel().findButton(e)) {
			mainController.getOptionsPanel().mouseClicked(e);
			return;
		}
		}
	}

	public void mouseEntered(MouseEvent e) {
		if(mainController.getMainPanel() != null) {
		if(mainController.getMainPanel().getCheckersPanel().findButton(e))
		mainController.getMainPanel().getCheckersPanel().mouseEntered(e);
		}
	}

	public void mouseExited(MouseEvent e) {
		if(mainController.getMainPanel() != null) {
		if(mainController.getMainPanel().getCheckersPanel().findButton(e))
		mainController.getMainPanel().getCheckersPanel().mouseExited(e);
		}
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

}
