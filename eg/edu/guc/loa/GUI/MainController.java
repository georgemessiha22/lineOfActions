package eg.edu.guc.loa.GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.JPanel;

import eg.edu.guc.loa.GUI.mainpanel.MainPanel;
import eg.edu.guc.loa.GUI.otherpanel.EndPage;
import eg.edu.guc.loa.GUI.otherpanel.OptionsPanel;
import eg.edu.guc.loa.GUI.otherpanel.StartPage;
import eg.edu.guc.loa.engine.Board;

@SuppressWarnings("serial")
public class MainController extends JPanel {
	private Handler handler;
	private Board board;
	private MainPanel mainPanel;
	private StartPage startPage;
	private OptionsPanel optionsPanel;
	private EndPage endPage;


	public MainController() {
		this.setOpaque(false);
		this.setLayout(new CardLayout());
		handler = new Handler(this);
		board = new Board();
	}

	public void setBackGrounds(Color c) {
		if (c == null) {
			this.setOpaque(false);
			repaint();
		} else {
			this.setOpaque(true);
			this.setBackground(c);
			repaint();
		}
	}

	public void setMainPanelVisible() {
		if(this.mainPanel == null) {
			mainPanel = new MainPanel(this);
			this.add(mainPanel, BorderLayout.CENTER);
		}
		this.mainPanel.setVisible(true);
		if(this.startPage != null) {
		this.startPage.setVisible(false);
		}
		if(this.optionsPanel != null) {
		this.optionsPanel.setVisible(false);
		}
		if(this.endPage != null){
			this.endPage.setVisible(false);
		}
		this.endPage = null;
		repaint();
	}

	public void setOptionsPanelVisible() {
		if(this.optionsPanel == null)  {
			optionsPanel = new OptionsPanel(this);
			this.add(optionsPanel, BorderLayout.CENTER);
		}
		if (this.mainPanel != null){
			this.mainPanel.setVisible(false);
			}
			if(this.startPage != null){
			this.startPage.setVisible(false);
			}
			if(this.endPage != null){
				this.endPage.setVisible(false);
			}
			this.endPage = null;
		this.optionsPanel.setVisible(true);
		repaint();
	}

	public void setStartPageVisible() {
		if(this.startPage == null){
			startPage = new StartPage(this);
			this.add(startPage, BorderLayout.CENTER);
		}
		this.startPage.setVisible(true);
		if (this.mainPanel != null){
		this.mainPanel.setVisible(false);
		}
		if(this.optionsPanel != null){
		this.optionsPanel.setVisible(false);
		}
		if(this.endPage != null){
			this.endPage.setVisible(false);
		}
		this.endPage = null;
		repaint();
	}
	
public void setEndPageVisible(){
	if(this.endPage == null){
		endPage = new EndPage(this);
		this.add(endPage, BorderLayout.CENTER);
	}
	this.endPage.setVisible(true);
	if (this.mainPanel != null){
	this.mainPanel.setVisible(false);
	}
	if(this.startPage != null){
		this.startPage.setVisible(false);
		}
	if(this.optionsPanel != null){
	this.optionsPanel.setVisible(false);
	}
	repaint();
}

	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public MainPanel getMainPanel() {
		return mainPanel;
	}

	public OptionsPanel getOptionsPanel() {
		return optionsPanel;
	}

	public StartPage getStartPage() {
		return startPage;
	}

	public void applyChanges() {
		if(mainPanel != null){
		mainPanel.applyChanges();
		} 
		if(this.optionsPanel != null){
		this.optionsPanel.applyChanges();
		}
	}

}
