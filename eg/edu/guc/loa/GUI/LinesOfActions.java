package eg.edu.guc.loa.GUI;

import java.awt.Toolkit;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

@SuppressWarnings("serial")
public class LinesOfActions extends JFrame {

	public LinesOfActions() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException ex) {
		} catch (InstantiationException ex) {
		} catch (IllegalAccessException ex) {
		} catch (UnsupportedLookAndFeelException ex) {
		}
		try {
			this.setIconImage(ImageIO.read(getClass().getResource(
					"/eg/edu/guc/loa/Resources/imageIcon.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.setTitle("Lines Of Actions");
		setUndecorated(true);
		Toolkit tk = Toolkit.getDefaultToolkit();
		int xSize = ((int) tk.getScreenSize().getWidth() - 15);
		int ySize = ((int) tk.getScreenSize().getHeight()) - 55;
		if ((int) tk.getScreenSize().getWidth() < ((int) tk.getScreenSize()
				.getHeight()))
			this.setSize(xSize, xSize);
		else
			this.setSize(ySize, ySize);
		this.add(new BackgroundPanel(this));
		this.setMaximumSize(this.getSize());
		this.setVisible(true);
	}

	public void setSize(int x, int y) {
		Toolkit tk = Toolkit.getDefaultToolkit();
		int xLocation = (((int) tk.getScreenSize().getWidth()) - x) / 2;
		int yLocation = (((int) tk.getScreenSize().getHeight()) - y - 40) / 2;
		this.setLocation(xLocation, yLocation);
		super.setSize(x, y);
		 this.setResizable(false);
	}

	void fullScreen(){
		if(this.getExtendedState() == 0){
		this.setExtendedState(6);
		} else {
			this.setExtendedState(0);
		}
	}
}
