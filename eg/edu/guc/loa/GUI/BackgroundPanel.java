package eg.edu.guc.loa.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BackgroundPanel extends JPanel {
	JLabel Exit;
	JLabel fullScreen;
	LinesOfActions linesOfActions;
	Image imageOrg = null;
	Image image = null;
	private MainController x;
	{
		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				int w = BackgroundPanel.this.getWidth();
				int h =BackgroundPanel.this.getHeight();
				image = w > 0 && h > 0 ? imageOrg.getScaledInstance(w, h,
						java.awt.Image.SCALE_SMOOTH) : imageOrg;
				BackgroundPanel.this.repaint();
			}
		});
	}
	
	public BackgroundPanel (LinesOfActions linesOfActions){
		try {
			image = ImageIO.read(getClass().getResource("/eg/edu/guc/loa/Resources/images.jpg"));
			imageOrg = ImageIO.read(getClass().getResource("/eg/edu/guc/loa/Resources/images.jpg"));
			this.setOpaque(false);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,
					"Can't load Images Error :" + e.getStackTrace(), "Warning",
					JOptionPane.OK_OPTION);
		} catch(IllegalArgumentException e){
			
		}
		this.linesOfActions= linesOfActions;
		this.setLayout(new BorderLayout());
		// full screen and Exit buttons at the top
		JPanel temp = new JPanel(new BorderLayout());
		temp.setOpaque(false);
		Exit = new JLabel("X");
		Exit.setFont(new Font("l",Font.ITALIC,20));
		Exit.setForeground(Color.RED);
		Exit.addMouseListener(new theHandler());
		Exit.setToolTipText("Exit Game");
		
		fullScreen = new JLabel(" []");
		fullScreen.setFont(new Font("l",Font.ITALIC,20));
		fullScreen.setForeground(Color.RED);
		fullScreen.addMouseListener(new theHandler());
		fullScreen.setToolTipText("FullScreen");
		temp.add(Exit,BorderLayout.LINE_END);
		temp.add(fullScreen,BorderLayout.LINE_START);
		this.add(temp,BorderLayout.NORTH);
		
		
		//the Game Panel
		 x = new MainController();
		x.setStartPageVisible();
		this.add(x,BorderLayout.CENTER);
	}
	
	public void paint(Graphics g) {
		if (image != null)
			g.drawImage(image, 0, 0, null);
		super.paint(g);
	}
	
	class theHandler implements MouseListener{


		public void mouseClicked(MouseEvent e) {
			if(e.getSource() == Exit){
				x.setVisible(false);
				int result = JOptionPane.showConfirmDialog(x, "Exit the Game?","Warning",JOptionPane.YES_NO_OPTION);
			if(result == JOptionPane.YES_OPTION){
				System.exit(0);
			}else{
				x.setVisible(true);
			}
			}
			if(e.getSource() == fullScreen){
				linesOfActions.fullScreen();
			}
		}


		public void mouseEntered(MouseEvent e) {
			if(e.getSource()==Exit){
				Exit.setFont(new Font("l",Font.BOLD,26));
			}
			if(e.getSource()==fullScreen){
				fullScreen.setFont(new Font("l",Font.BOLD,26));
			}
		}

		public void mouseExited(MouseEvent e) {
			if(e.getSource()==Exit){
				Exit.setFont(new Font("l",Font.ITALIC,20));
			}
			if(e.getSource()==fullScreen){
				fullScreen.setFont(new Font("l",Font.ITALIC,20));
			}
		}


		public void mousePressed(MouseEvent e) {
			
		}


		public void mouseReleased(MouseEvent e) {

		}
		
	}
}
