package eg.edu.guc.loa.GUI.otherpanel;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import eg.edu.guc.loa.GUI.MainController;
import eg.edu.guc.loa.GUI.stuff.PlainJButton;
import eg.edu.guc.loa.GUI.stuff.ScoreTextField;

@SuppressWarnings("serial")
public class EndPage extends JPanel{
	private MainController mainController;
	private JLabel GameOver;
	private ScoreTextField player1;
	private ScoreTextField player2;
	private JLabel playAgain;
	private JLabel cong;
	
	public EndPage(MainController mainController){
		this.setLayout(new GridLayout(0,1));
		this.setOpaque(false);
		this.mainController = mainController;
		this.add(new PlainJButton("  "));
		//game over word 
		JPanel temp = new JPanel (new FlowLayout());
		temp.setOpaque(false);
		GameOver = new JLabel("GAME OVER!!");
		GameOver.setFont(new Font("L",Font.BOLD,56));
		GameOver.setForeground(Color.RED.darker());
		temp.add(GameOver);
		this.add(temp);
		
		JPanel temp1 = new JPanel (new FlowLayout());
		temp1.setOpaque(false);
		String s ="CONGRATULATIONS";
		cong = new JLabel(s);
		cong.setFont(new Font("L",Font.BOLD,26));
		cong.setForeground(Color.BLUE.darker());
		temp1.add(cong);
		this.add(temp1);
		
		if(this.mainController.getBoard().getWinnerPlayer() != null){
		JPanel temp4 = new JPanel (new FlowLayout());
		temp4.setOpaque(false);
		 s =this.mainController.getBoard().getWinnerPlayer().getName()+" Won.";
		JLabel x = new JLabel(s);
		x.setFont(new Font("L",Font.BOLD,26));
		x.setForeground(this.mainController.getBoard().getWinnerPlayer().getColor().darker().darker());
		temp4.add(x);
		this.add(temp4);
		} else {
			JPanel temp4 = new JPanel (new FlowLayout());
			temp4.setOpaque(false);
			 s ="It's a Tie.";
			 JLabel x = new JLabel(s);
			x.setFont(new Font("L",Font.BOLD,26));
			x.setForeground(this.mainController.getBoard().getWinnerPlayer().getColor().darker().darker());
			temp4.add(x);
			this.add(temp4);
		}
		
		JPanel temp3 = new JPanel (new FlowLayout());
		temp3.setOpaque(false);
		player1 =new ScoreTextField(this.mainController.getBoard().getFirstPlayer());
		temp3.add(player1);
		player2 =new ScoreTextField(this.mainController.getBoard().getSecondPlayer());
		temp3.add(player2);
		this.add(temp3);
		
		JPanel temp2 = new JPanel (new FlowLayout());
		temp2.setOpaque(false);
		playAgain = new JLabel("Play Again");
		playAgain.setFont(new Font("l" , Font.ITALIC,26));
		playAgain.addMouseListener(new theHandler());
		playAgain.setForeground(Color.PINK);
		temp2.add(playAgain);
		this.add(temp2);
	}
	class theHandler implements MouseListener{


		public void mouseClicked(MouseEvent e) {
			if(e.getSource()== playAgain){
				mainController.getMainPanel().reset();
				mainController.setMainPanelVisible();
			}
		}

		public void mouseEntered(MouseEvent e) {
			if(e.getSource()== playAgain) {
				playAgain.setFont(new Font("l" , Font.BOLD,46));
			}
		}


		public void mouseExited(MouseEvent e) {
			if(e.getSource()== playAgain) {
				playAgain.setFont(new Font("l" , Font.ITALIC,26));
			}
		}


		public void mousePressed(MouseEvent e) {
			
		}

	public void mouseReleased(MouseEvent e) {
			
		}
		
	}
}
