package eg.edu.guc.loa.GUI.otherpanel;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import eg.edu.guc.loa.GUI.Handler;
import eg.edu.guc.loa.GUI.MainController;
import eg.edu.guc.loa.GUI.stuff.PlainJButton;
import eg.edu.guc.loa.GUI.stuff.PlayerOptionsPanel;

@SuppressWarnings("serial")
public class StartPage extends JPanel {
	private JPanel holder;
	private JButton start;
	private MainController mainController;
	private PlayerOptionsPanel firstplayer;
	private PlayerOptionsPanel secondplayer;

	public StartPage(MainController mainController) {
		this.setOpaque(false);
		this.setLayout(new BorderLayout());
		this.mainController = mainController;
		holder = new JPanel(new GridLayout(0, 1));
		holder.setOpaque(false);
		holder.add(new PlainJButton(" " ));
		populateFirstPlayerOptions();
		populateSecondPlayerOptions();
		holder.add(new PlainJButton(" " ));
		populateStart();
		addMouseListener();
		this.add(holder);
	}

	private void populateFirstPlayerOptions() {
		firstplayer = new PlayerOptionsPanel(mainController.getBoard()
				.getFirstPlayer(),1);
		holder.add(firstplayer);
	}

	private void populateSecondPlayerOptions() {
		secondplayer = new PlayerOptionsPanel(mainController.getBoard()
				.getSecondPlayer(),2);
		holder.add(secondplayer);
	}

	private void populateStart() {
		start = new JButton("Start Game");
		start.setName("start");
		start.setFocusable(false);
		start.setBorderPainted(false);
		JPanel temp = new JPanel(new GridLayout(1, 0));
		temp.setOpaque(false);
		temp.add(new PlainJButton("  "));
		temp.add(new PlainJButton("  "));
		temp.add(start);
		temp.add(new PlainJButton("  "));
		temp.add(new PlainJButton("  "));
		this.add(temp, BorderLayout.SOUTH);
	}

	public boolean findButton(MouseEvent e) {
		if (e.getSource() == start)
			return true;
		return false;
	}

	public JButton getButton(MouseEvent e) {
		if (e.getSource() == start)
			return start;
		return null;
	}

	public JButton getButton(String name) {
		if (name.equalsIgnoreCase(start.getName()))
			return start;
		return null;
	}

	public void setVisible(Handler handler) {
		this.setVisible(true);
	}

	public void setHidden() {
		this.setVisible(false);
	}

	public void mouseClicked(MouseEvent e) {
		if (getButton(e) != null) {
			if (getButton(e).getName().equalsIgnoreCase("start")) {
				if (actionPerformed()) {
					this.mainController.applyChanges();
					this.mainController.setMainPanelVisible();
				}
			}
		}
	}

	public void addMouseListener() {
		this.addMouseListener(this.mainController.getHandler());
		this.start.addMouseListener(this.mainController.getHandler());
	}

	public boolean actionPerformed() {
		if (firstplayer.getNewName()
				.equalsIgnoreCase(secondplayer.getNewName())) {
			JOptionPane.showMessageDialog(null, "Players have the same Name.",
					" Warning unable to start", JOptionPane.OK_OPTION);
			return false;
		}
		if (firstplayer.getNewColor() == secondplayer.getNewColor()) {
			JOptionPane.showMessageDialog(null, "Players have the same Color.",
					" Warning unable to Start Game", JOptionPane.OK_OPTION);
			return false;
		}
		firstPlayerChanges();
		secondPlayerChanges();
		return true;
	}

	private void firstPlayerChanges() {
		mainController.getBoard().getFirstPlayer()
				.setName(firstplayer.getNewName());
		mainController.getBoard().getFirstPlayer()
				.setColor(firstplayer.getNewColor());
	}
	
	private void secondPlayerChanges() {
		mainController.getBoard().getSecondPlayer()
				.setName(secondplayer.getNewName());
		mainController.getBoard().getSecondPlayer()
				.setColor(secondplayer.getNewColor());
	}
	
}
