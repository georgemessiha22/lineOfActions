package eg.edu.guc.loa.GUI.otherpanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eg.edu.guc.loa.GUI.Handler;
import eg.edu.guc.loa.GUI.MainController;
import eg.edu.guc.loa.GUI.stuff.BoardOptions;
import eg.edu.guc.loa.GUI.stuff.ColorList;
import eg.edu.guc.loa.GUI.stuff.PlayerOptionsPanel;

@SuppressWarnings("serial")
public class OptionsPanel extends JPanel {
	private MainController mainController;
	private JButton cancel;
	private JButton save;
	private JPanel buttons;
	private PlayerOptionsPanel firstplayer;
	private PlayerOptionsPanel secondplayer;
	private ColorList backgroundColor;
	private BoardOptions boardOptions;
	private JPanel holder;
	private JComboBox<String> sizeOption;
	private Color previousBackgroundColor;
	public final static String[] sizes = { "3x3", "4x4", "5x5", "6x6", "7x7",
			"8x8", "9x9", "10x10", "11x11", "12x12", "13x13", "14x14", "15x15",
			"16x16" };
	public final static int[] size = { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
			15, 16 };

	public OptionsPanel(MainController mainController) {
		this.setOpaque(false);
		this.setLayout(new BorderLayout());
		this.mainController = mainController;
		this.holder = new JPanel(new GridLayout(0, 1));
		this.holder.setOpaque(false);
		populateFirstPlayerOptions();
		populateSecondPlayerOptions();
		populateChangeBackGroundColor();
		boardOptions = new BoardOptions(this.mainController);
		this.holder.add(boardOptions);
		this.buttons = new JPanel();
		buttons.setOpaque(false);
		this.add(holder, BorderLayout.CENTER);
		this.buttons.setLayout(new FlowLayout());
		populateSave();
		populateCancel();
		this.add(buttons, BorderLayout.SOUTH);
		addMouseListener();
	}

	private void populateChangeBackGroundColor() {
		JPanel temp = new JPanel();
		theHandler x = new theHandler();
		temp.setOpaque(false);
		JTextField text = new JTextField("Background Color :");
		text.setFont(new Font("Verdana", Font.ITALIC, 20));
		text.setEditable(false);
		text.setBorder(null);
		text.setOpaque(false);
		if (this.mainController.isOpaque()) {
			backgroundColor = new ColorList(this.mainController.getBackground());
			backgroundColor.setSelected(this.mainController.getBackground());
		} else {
			backgroundColor = new ColorList(null);
		}
		this.previousBackgroundColor = this.mainController.getBackground();
		backgroundColor.addActionListener(x);
		temp.add(text);
		temp.add(backgroundColor);

		// adding board size option
		text = new JTextField("Board Size :");
		text.setFont(new Font("Verdana", Font.ITALIC, 20));
		text.setEditable(false);
		text.setBorder(null);
		text.setOpaque(false);
		sizeOption = new JComboBox<String>(sizes);
		for (int i = 0; i < size.length; i++) {
			if (mainController.getBoard().getSize() == size[i]) {
				sizeOption.setSelectedIndex(i);
				break;
			}
		}
		temp.add(text);
		temp.add(sizeOption);
		sizeOption.addActionListener(x);

		this.holder.add(temp);
	}

	private void populateFirstPlayerOptions() {
		firstplayer = new PlayerOptionsPanel(mainController.getBoard()
				.getFirstPlayer(), 1);
		this.holder.add(firstplayer);
	}

	private void populateSecondPlayerOptions() {
		secondplayer = new PlayerOptionsPanel(mainController.getBoard()
				.getSecondPlayer(), 2);
		this.holder.add(secondplayer);
	}

	private void populateCancel() {
		cancel = new JButton("Cancel");
		cancel.setName("cancel");
		cancel.setFocusable(false);
		cancel.setBorderPainted(false);
		this.buttons.add(cancel);
	}

	private void populateSave() {
		save = new JButton("Save");
		save.setName("save");
		save.setFocusable(false);
		save.setBorderPainted(false);
		this.buttons.add(save);
	}

	public boolean findButton(MouseEvent e) {
		if (e.getSource() == save)
			return true;
		if (e.getSource() == cancel)
			return true;
		return false;
	}

	public JButton getButton(MouseEvent e) {
		if (e.getSource() == cancel)
			return cancel;
		if (e.getSource() == save)
			return save;
		return null;
	}

	public JButton getButton(String name) {
		if (name.equalsIgnoreCase(cancel.getName()))
			return cancel;
		if (name.equalsIgnoreCase(save.getName()))
			return save;
		return null;
	}

	public void mouseClicked(MouseEvent e) {
		if (getButton(e) != null) {
			if (getButton(e).getName().equalsIgnoreCase("cancel")) {
				this.mainController.setBackground(this.previousBackgroundColor);
				this.boardOptions.reset();
				this.firstplayer.reset(this.mainController.getBoard()
						.getFirstPlayer());
				this.secondplayer.reset(this.mainController.getBoard()
						.getSecondPlayer());
				if (this.mainController.isOpaque()) {
					backgroundColor.setSelected(this.mainController.getBackground());
				} else {
					backgroundColor.setSelectedIndex(0);
				}
				for (int i = 0; i < size.length; i++) {
					if (mainController.getBoard().getSize() == size[i]) {
						sizeOption.setSelectedIndex(i);
						break;
					}
				}this.mainController.setMainPanelVisible();
			} else {
				if (getButton(e).getName().equalsIgnoreCase("save")) {
					if (this.actionPerformed()) {
						this.mainController.applyChanges();
						this.mainController.setMainPanelVisible();
						this.boardOptions.reset();
						this.firstplayer.reset(this.mainController.getBoard()
								.getFirstPlayer());
						this.secondplayer.reset(this.mainController.getBoard()
								.getSecondPlayer());
					}
				}
			}
		}
	}

	public void addMouseListener() {
		this.cancel.addMouseListener(this.mainController.getHandler());
		this.save.addMouseListener(this.mainController.getHandler());
	}

	public void setVisible(Handler handler) {
		this.setVisible(true);
	}

	public void setHidden() {
		this.setVisible(false);
	}

	public boolean actionPerformed() {
		if (firstplayer.getNewName()
				.equalsIgnoreCase(secondplayer.getNewName())) {
			JOptionPane.showMessageDialog(null, "Players have the same Name.",
					" Warning unable to start", JOptionPane.OK_OPTION);
			return false;
		}
		if (firstplayer.getNewColor() == secondplayer.getNewColor()) {
			JOptionPane.showMessageDialog(null, "Players have the same Color.",
					" Warning unable to Start Game", JOptionPane.OK_OPTION);
			return false;
		}
		if (this.boardOptions.getColor1() == this.boardOptions.getColor2()) {
			JOptionPane.showMessageDialog(null,
					"Can not Have Board of Same Color.",
					" Warning unable to Start Game", JOptionPane.OK_OPTION);
			return false;
		}
		if (this.boardOptions.getColor1() == firstplayer.getNewColor()
				|| secondplayer.getNewColor() == this.boardOptions.getColor1()
				|| this.boardOptions.getColor2() == secondplayer.getNewColor()
				|| this.boardOptions.getColor2() == firstplayer.getNewColor()) {
			JOptionPane.showMessageDialog(null,
					"Player can not have same Color as Board ",
					" Warning unable to Start Game", JOptionPane.OK_OPTION);
			return false;
		}
		if (this.boardOptions.getSelectionColor() == firstplayer.getNewColor()
				|| secondplayer.getNewColor() == this.boardOptions
						.getSelectionColor()
				|| this.boardOptions.getPossibleMoveColor() == secondplayer
						.getNewColor()
				|| this.boardOptions.getPossibleMoveColor() == firstplayer
						.getNewColor()) {
			JOptionPane
					.showMessageDialog(
							null,
							"Player can not have same Color as Selection Color Nor Possible Color ",
							" Warning unable to Start Game",
							JOptionPane.OK_OPTION);
			return false;
		}
		if (this.boardOptions.getSelectionColor() == this.boardOptions
				.getColor1()
				|| this.boardOptions.getColor2() == this.boardOptions
						.getSelectionColor()
				|| this.boardOptions.getPossibleMoveColor() == this.boardOptions
						.getColor1()
				|| this.boardOptions.getPossibleMoveColor() == this.boardOptions
						.getColor2()) {
			JOptionPane
					.showMessageDialog(
							null,
							"Board can not have same Color as Selection Color Nor Possible Color ",
							" Warning unable to Start Game",
							JOptionPane.OK_OPTION);
			return false;
		}
		if (size[this.sizeOption.getSelectedIndex()] != mainController
				.getBoard().getSize()) {
			int yes = JOptionPane.showConfirmDialog(null,
					"Changing Board size will start new game", " Warning",
					JOptionPane.YES_NO_OPTION);
			if (yes == JOptionPane.YES_OPTION) {
				mainController.getBoard().setSize(
						size[this.sizeOption.getSelectedIndex()]);
				mainController.getMainPanel().reset();
			}
		}
		firstPlayerChanges();
		secondPlayerChanges();
		boardChanges();
		return true;
	}

	private void firstPlayerChanges() {
		mainController.getBoard().getFirstPlayer()
				.setName(firstplayer.getNewName());
		mainController.getBoard().getFirstPlayer()
				.setColor(firstplayer.getNewColor());
	}

	private void boardChanges() {
		this.mainController.getMainPanel().getCheckersPanel()
				.setColor1(this.boardOptions.getColor1());
		this.mainController.getMainPanel().getCheckersPanel()
				.setColor2(this.boardOptions.getColor2());
		this.mainController.getMainPanel().getCheckersPanel()
				.setPossibleMoveColor(this.boardOptions.getPossibleMoveColor());
		this.mainController.getMainPanel().getCheckersPanel()
				.setSelectedColor(this.boardOptions.getSelectionColor());
		if (this.mainController.getMainPanel().getPosition() != this.boardOptions
				.getPanelPostion()) {
			this.mainController.getMainPanel().changePosition();
		}
	}

	private void secondPlayerChanges() {
		mainController.getBoard().getSecondPlayer()
				.setName(secondplayer.getNewName());
		mainController.getBoard().getSecondPlayer()
				.setColor(secondplayer.getNewColor());
	}

	public void applyChanges() {
		this.removeAll();
		this.holder = new JPanel(new GridLayout(0, 1));
		this.holder.setOpaque(false);
		this.firstplayer.reset(mainController.getBoard().getFirstPlayer());
		this.secondplayer.reset(mainController.getBoard().getSecondPlayer());
		this.holder.add(this.firstplayer);
		this.holder.add(this.secondplayer);
		this.populateChangeBackGroundColor();
		boardOptions = new BoardOptions(this.mainController);
		this.holder.add(boardOptions);
		this.add(holder, BorderLayout.CENTER);
		this.add(buttons, BorderLayout.SOUTH);
		this.repaint();
	}

	class theHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (backgroundColor.getSelectedIndex() == 0) {
				mainController.setBackGrounds(null);
			} else {
				mainController.setBackGrounds(backgroundColor.getSelected());
			}
		}
	}
}
