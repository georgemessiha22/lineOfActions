package eg.edu.guc.loa.engine.stuff;

import eg.edu.guc.loa.engine.Board;

public class LinkList {
	private Link<Board> head;

	public LinkList() {
		head = null;
	}

	public void insertFirst(Board o) {
		Link<Board> newLink_Comparable = new Link<Board>(o);
		newLink_Comparable.next = head;
		head = newLink_Comparable;
	}

	public Board removeFirst() {
		if (head == null)
			return null;
		Board res = head.data;
		head = head.next;
		return res;
	}

	public Board getFirst() {
		return head.data;
	}

	public void insertLast(Board o) {
		Link<Board> newLink_Board = new Link<Board>(o);
		if (head == null) {
			head = newLink_Board;
			return;
		}
		Link<Board> current = head;
		while (current.next != null) {
			current = current.next;
		}
		current.next = newLink_Board;
	}

	public Board removeLast() {
		if (head == null)
			return null;
		if (head.next == null) {
			Board res = head.data;
			head = null;
			return res;
		}
		Link<Board> current = head;
		while (current.next.next != null)
			current = current.next;
		Board res = current.next.data;
		current.next = null;
		return res;
	}

	public Board getLast() {
		if (head == null)
			return null;
		Link<Board> current = head;
		while (current.next != null)
			current = current.next;
		return current.data;
	}

	public Board peekLast() {
		if (head == null)
			return null;
		Link<Board> current = head;
		while (current.next != null)
			current = current.next;
		Board b = new Board();
		b.setEqual(current.data);
		return b;
	}

	public boolean isEmpty() {
		return (head == null);
	}

	public Board get(int i) {
		if (i == 0)
			return null;
		Link<Board> pointer = head;
		i--;
		while (i != 0 && pointer.next != null) {
			pointer = pointer.next;
			i--;
		}
		return pointer.data;
	}

	public void remove(Board i) {
		Link<Board> prev = head;
		Link<Board> pointer = head;
		while (pointer != null) {
			if (pointer.data.equal(i)) {
				if (prev.data.equal(pointer.data)) {
					this.removeFirst();
				} else {
					prev.next = pointer.next;
				}
				break;
			}
			prev = pointer;
			pointer = pointer.next;
		}

	}

	public int size() {
		if (head == null) {
			return 0;
		}
		Link<Board> pointer = head;
		int counter = 1;
		while (pointer.next != null) {
			pointer = pointer.next;
			counter++;
		}
		return counter;
	}
}

@SuppressWarnings("hiding")
class Link<Board> {
	public Board data;
	public Link<Board> next;

	public Link(Board o) {
		this.data = o;
		this.next = null;
	}
}
