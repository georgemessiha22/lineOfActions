package eg.edu.guc.loa.engine.stuff;

import java.awt.Color;

import eg.edu.guc.loa.engine.Point;


public class Checker implements Comparable<Checker> {
	private Color color;
	private Point position;

	public Checker(final Color color, final Point position) {
		this.color = color;
		this.position = position;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(final Color color) {
		this.color = color;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(final Point position) {
		this.position = position;
	}

	public String toString() {
		if (this.color.equals(Color.BLACK)) {
			return "Black";
		}
		return "White";
	}

	public int compareTo(final Checker o) {
		if (o.getPosition().compareTo(getPosition()) == 0) {
			return 0;
		}
		return -1;
	}

}
