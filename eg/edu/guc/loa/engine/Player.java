package eg.edu.guc.loa.engine;

import java.awt.Color;
import java.util.ArrayList;

import eg.edu.guc.loa.engine.stuff.Checker;

public class Player implements Comparable<Player> {
	private String name;
	private Checker[] checkers;
	private Color color;
	private int score =0;

	public void reset(){
		this.checkers = new Checker[0];
	}
	
	public Player(final String name) {
		this.name = name;
		this.checkers = new Checker[0];
	}

	public Player(final String name, final Color color) {
		this.name = name;
		this.color = color;
		this.checkers = new Checker[0];
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void addChecker(final Checker checker) {
		Checker[] newCheckers = new Checker[this.checkers.length + 1];
		for (int i = 0; i < this.checkers.length; i++) {
			newCheckers[i] = this.checkers[i];
		}
		newCheckers[this.checkers.length] = checker;
		this.checkers = newCheckers;
	}

	public void removeChecker(final Point p) {
		Checker[] newCheckers = new Checker[this.checkers.length - 1];
		for (int i = 0; i < this.checkers.length; i++) {
			if (this.checkers[i].getPosition().compareTo(p) != 0) {
				for (int j = 0; j < newCheckers.length; j++) {
					if (newCheckers[j] == null) {
						newCheckers[j] = this.checkers[i];
						break;
					}
				}
			}
		}
		this.checkers = newCheckers;
	}

	public void setColor(final Color color) {
		this.color = color;
	}

	public Color getColor() {
		return this.color;
	}

	public void moveChecker(final Point position, final Point newPosition) {
		getChecker(position).setPosition(newPosition);
	}

	public Checker getChecker(final Point position) {
		for (int i = 0; i < this.checkers.length; i++) {
			if (this.checkers[i].getPosition().compareTo(position) == 0) {
				return this.checkers[i];
			}
		}
		return null;
	}

	public Checker[] getCheckers() {
		return this.checkers;
	}

	public boolean isDead() {
		if (this.checkers.length == 0) {
			return true;
		}
		return false;
	}

	public int compareTo(final Player compare) {
		if (this.getName().equalsIgnoreCase(compare.getName())) {
			return 0;
		} else {
			return -1;
		}
	}

	public boolean isWinner() {
		if (!this.isDead() && isConnected()) {
			return true;
		}
		return false;
	}

	private boolean isConnected() {
		ArrayList<Checker> p = new ArrayList<Checker>();
		for (int i = 0; i < checkers.length; i++) {
			p.add(checkers[i]);
		}
		ArrayList<Checker> connected = new ArrayList<Checker>();
		connected.add(p.get(0));
		for (int i = 0; i < connected.size(); i++) {
			for (int k = 1; k < p.size(); k++) {
				Point temp = connected.get(i).getPosition();
				if (inArea(temp, p.get(k).getPosition())
						&& !contain(connected, p.get(k))) {
					connected.add(p.get(k));
				}
			}
		}
		if (connected.size() == p.size()) {
			return true;
		}
		return false;
	}

	private boolean contain(final ArrayList<Checker> c, final Checker c1) {
		for (int i = 0; i < c.size(); i++) {
			if (c1.compareTo(c.get(i)) == 0) {
				return true;
			}
		}
		return false;
	}

	public boolean equal(Player p) {
		if (this.compareTo(p) == 0)
			return true;
		return false;
	}

	private boolean inArea(final Point p1, final Point p2) {
		if (p2.compareTo(new Point(p1.getX() + 1, p1.getY())) == 0
				|| p2.compareTo(new Point(p1.getX() + 1, p1.getY() + 1)) == 0
				|| p2.compareTo(new Point(p1.getX(), p1.getY() + 1)) == 0
				|| p2.compareTo(new Point(p1.getX() - 1, p1.getY() + 1)) == 0
				|| p2.compareTo(new Point(p1.getX() - 1, p1.getY())) == 0
				|| p2.compareTo(new Point(p1.getX() - 1, p1.getY() - 1)) == 0
				|| p2.compareTo(new Point(p1.getX(), p1.getY() - 1)) == 0
				|| p2.compareTo(new Point(p1.getX() + 1, p1.getY() - 1)) == 0) {
			return true;
		}

		return false;
	}

	public void setEqual(Player player) {
		this.checkers = new Checker[(player.getCheckers().length)];
		for (int i = 0; i < this.checkers.length; i++) {
			checkers[i] = new Checker(color, new Point(player.getCheckers()[i]
					.getPosition().getX(), player.getCheckers()[i]
					.getPosition().getY()));
		}
		this.name = player.name;
		this.score = player.score;
		this.color = player.color;
	}

	public int getScore() {
		return score;
	}
	
	public void increaseScore(){
		score++;
	}
	
	public void decreaseScore(){
		score--;
	}
}
