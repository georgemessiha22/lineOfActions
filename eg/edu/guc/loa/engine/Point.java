
package eg.edu.guc.loa.engine;

public class Point implements Comparable<Point> {

	private int x;
	private int y;

	public Point(final int x, final int y) {
		this.x = x;
		this.y = y;
	}

	public Point() {
	}

	public int getX() {
		return x;
	}

	public void setX(final int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(final int y) {
		this.y = y;
	}

	public void move(final int xDelta, final int yDelta) {
		x = xDelta;
		y = yDelta;
	}

	public String draw() {
		return "(" + x + "," + y + ")";
	}

	public boolean equal(Point o) {
		if (this.getX() == o.getX() && this.getY() == o.getY()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int compareTo(final Point o) {
		if (this.getX() == o.getX() && this.getY() == o.getY()) {
			return 0;
		} else {
			return -1;
		}
	}

}
