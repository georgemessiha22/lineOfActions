package eg.edu.guc.loa.engine;

import java.awt.Color;
import java.util.ArrayList;

import eg.edu.guc.loa.engine.stuff.Checker;

public class Board implements BoardInterface, Comparable<Board> {
	private Player[][] board;
	private Player firstPlayer;
	private Player secondPlayer;
	private int turn = 1;
	private String name;
	private int size = 8;
	private static int counter = 0;

	public Board() {
		name = "Board " + counter;
		counter++;
		firstPlayer = new Player("Mariam Botros", Color.WHITE);
		secondPlayer = new Player("Shery Nazih", Color.BLACK);
		this.boardPopulate();
	}

	public void reset(){
		firstPlayer.reset();
		secondPlayer.reset();
		this.boardPopulate();
		this.turn = 1;
	}

	public int getSize() {
		return size;
	}

	public Player getFirstPlayer() {
		return firstPlayer;
	}

	public Player getSecondPlayer() {
		return secondPlayer;
	}

	public Player getPositionPlayer(final Point p) {
		return this.board[p.getY()][p.getX()];
	}

	public Player[][] getBoard() {
		return this.board;
	}

	public void setEqual(final Board b) {
		this.size = b.getSize();
		Player[][] newBoard = new Player[b.getBoard().length][b.getBoard().length];
		Player newFirstPlayer = new Player("" + b.getFirstPlayer().getName(), b
				.getFirstPlayer().getColor());
		Player newSecondPlayer = new Player("" + b.getSecondPlayer().getName(),
				b.getSecondPlayer().getColor());
		newFirstPlayer.setEqual(b.getFirstPlayer());
		newSecondPlayer.setEqual(b.getSecondPlayer());
		this.setFirstPlayer(newFirstPlayer);
		this.setSecondPlayer(newSecondPlayer);
		for (int i = 0; i < b.getBoard().length; i++) {
			for (int j = 0; j < b.getBoard().length; j++) {
				if (b.getBoard()[i][j] != null) {
					if (b.getBoard()[i][j].equal(newFirstPlayer)) {
						newBoard[i][j] = newFirstPlayer;
					} else {
						if (b.getBoard()[i][j].equal(newSecondPlayer)) {
							newBoard[i][j] = newSecondPlayer;
						}
					}
				}
			}
		}
		this.turn = b.getTurn();
		this.setBoard(newBoard);
	}

	public void setBoard(final Player[][] board) {
		this.board = board;
	}

	public void setFirstPlayer(final Player firstPlayer) {
		this.firstPlayer = firstPlayer;
	}

	public void setSecondPlayer(final Player secondPlayer) {
		this.secondPlayer = secondPlayer;
	}

	public void setSize(int size) {
		this.size = size;
	}

	private void boardPopulate() {
		this.board = new Player[size][size];
		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board.length; j++) {
				if ((i == 0 || i == this.board.length-1) && (j != 0 && j != this.board.length-1)) {
					this.add(firstPlayer, new Point(j, i));
				} else {
					if ((j == 0 || j == this.board.length-1) && (i != 0 && i != this.board.length-1)) {
						this.add(secondPlayer, new Point(j, i));
					}else {
						this.board[i][j]=null;
					}
				}
			}
		}
	}

	private void add(final Player s, final Point p) {
		if (s.compareTo(firstPlayer) == 0) {
			this.board[p.getY()][p.getX()] = firstPlayer;
			firstPlayer.addChecker(new Checker(firstPlayer.getColor(), p));
			return;
		}
		if (s.compareTo(secondPlayer) == 0) {
			this.board[p.getY()][p.getX()] = secondPlayer;
			secondPlayer.addChecker(new Checker(secondPlayer.getColor(), p));
		}
	}

	public boolean move(final Point start, final Point end) {
		if (!(isEmptyPosition(start))
				&& !(this.isGameOver())
				&& this.getPlayerTurn().compareTo(
						this.board[start.getY()][start.getX()]) == 0
				&& this.isValidMove(start, end)) {
			if (!(isEmptyPosition(end))) {
				if (this.board[end.getY()][end.getX()]
						.compareTo(getPlayerTurn()) == 0) {
					return false;
				} else
					this.board[end.getY()][end.getX()].removeChecker(end);
			}
			this.board[end.getY()][end.getX()] = this.board[start.getY()][start
					.getX()];
			getPlayerTurn().moveChecker(start, end);
			this.board[start.getY()][start.getX()] = null;
			changeTurn();
			return true;
		}
		return false;
	}

	private boolean isValidMove(final Point start, final Point end) {
		ArrayList<Point> temp = this.getPossibleMoves(start);
		for (int i = 0; i < temp.size(); i++) {
			if (end.compareTo(temp.get(i)) == 0)
				return true;
		}
		return false;
	}

	public boolean isGameOver() {
		if ( getWinner() != 0||firstPlayer.isDead() || !(this.hasPossibleMoves(firstPlayer))
				|| secondPlayer.isDead()
				|| !(this.hasPossibleMoves(secondPlayer)) )
			return true;
		return false;
	}

	public Player getWinnerPlayer(){
		if (firstPlayer.isWinner() && secondPlayer.isWinner()) {
			if (turn ==1){
				return secondPlayer;
			}
			if(turn == 2){
				return firstPlayer;
			}
		}
		if (firstPlayer.isWinner())
			return firstPlayer;
		if (secondPlayer.isWinner())
			return secondPlayer;
		return null;
	}
	
	public int getWinner() {
		if (firstPlayer.isWinner() && secondPlayer.isWinner()) {
			if (turn ==1){
				return 2;
			}
			if(turn == 2){
				return 1;
			}
		}
		if (firstPlayer.isWinner())
			return 1;
		if (secondPlayer.isWinner())
			return 2;
		return 0;
	}

	public int getColor(final Point p) {
		if (!(isEmptyPosition(p))) {
			if (this.board[p.getY()][p.getX()].getChecker(p).getColor() == firstPlayer
					.getColor()) {
				return 1;
			} else {
				if (this.board[p.getY()][p.getX()].getChecker(p).getColor() == secondPlayer
						.getColor())
					return 2;
			}
		}
		return 0;
	}

	public int getTurn() {
		return turn;
	}

	private void changeTurn() {
		this.turn = (this.turn == 1) ? 2 : 1;
	}

	public ArrayList<Point> getPossibleMoves(final Point start) {
		if (this.getPlayerTurn().compareTo(
				this.board[start.getY()][start.getX()]) == 0)
			return getPossibleMovesHelper(start);
		return new ArrayList<Point>();
	}

	public ArrayList<Point> getPossibleMovesHelper(final Point start) {
		ArrayList<Point> p = new ArrayList<Point>();
		addXPossibleMoves(p, start);
		addYPossibleMoves(p, start);
		addXYPossibleMoves(p, start);
		addYXPossibleMoves(p, start);
		return p;
	}

	private void addXPossibleMoves(final ArrayList<Point> p, final Point start) {
		int x = getPossibleMoveXAxis(start,
				this.board[start.getY()][start.getX()]);
		if (x != 0) {
			Point temp = new Point((start.getX() + x), start.getY());
			if (pointIsValid(temp)
					&& pathClear(start, temp, board[start.getY()][start.getX()])) {
				if (isEmptyPosition(temp)) {
					p.add(temp);
				} else {
					if (board[temp.getY()][temp.getX()].compareTo(board[start
							.getY()][start.getX()]) != 0)
						p.add(temp);
				}
			}
			temp = new Point((start.getX() - x), start.getY());
			if (pointIsValid(temp)
					&& pathClear(start, temp, board[start.getY()][start.getX()])) {
				if (isEmptyPosition(temp)) {
					p.add(temp);
				} else {
					if (board[temp.getY()][temp.getX()].compareTo(board[start
							.getY()][start.getX()]) != 0)
						p.add(temp);
				}
			}
		}
	}

	private int getPossibleMoveXAxis(final Point start, final Player player) {
		int counter = 0;
		Point temp = new Point(start.getX(), start.getY());
		while (pointIsValid(temp)) {
			if (board[temp.getY()][temp.getX()] != null)
				counter++;
			temp.setX(temp.getX() + 1);
		}
		temp = new Point(start.getX() - 1, start.getY());
		while (pointIsValid(temp)) {
			if (board[temp.getY()][temp.getX()] != null)
				counter++;
			temp.setX(temp.getX() - 1);
		}
		return counter;
	}

	private void addYPossibleMoves(final ArrayList<Point> p, final Point start) {
		int y = getPossibleMoveYAxis(start,
				this.board[start.getY()][start.getX()]);
		if (y != 0) {
			Point temp = new Point((start.getX()), (start.getY() + y));
			if (pointIsValid(temp)
					&& pathClear(start, temp, board[start.getY()][start.getX()])) {
				if (isEmptyPosition(temp)) {
					p.add(temp);
				} else {
					if (board[temp.getY()][temp.getX()].compareTo(board[start
							.getY()][start.getX()]) != 0)
						p.add(temp);
				}
			}
			temp = new Point((start.getX()), (start.getY() - y));
			if (pointIsValid(temp)
					&& pathClear(start, temp, board[start.getY()][start.getX()])) {
				if (isEmptyPosition(temp)) {
					p.add(temp);
				} else {
					if (board[temp.getY()][temp.getX()].compareTo(board[start
							.getY()][start.getX()]) != 0)
						p.add(temp);
				}
			}
		}
	}

	private int getPossibleMoveYAxis(final Point start, final Player player) {
		int counter = 0;
		Point temp = new Point(start.getX(), start.getY());
		while (pointIsValid(temp)) {
			if (board[temp.getY()][temp.getX()] != null)
				counter++;
			temp.setY(temp.getY() + 1);
		}
		temp = new Point(start.getX(), start.getY() - 1);
		while (pointIsValid(temp)) {
			if (board[temp.getY()][temp.getX()] != null)
				counter++;
			temp.setY(temp.getY() - 1);
		}
		return counter;
	}

	private void addYXPossibleMoves(final ArrayList<Point> p, final Point start) {
		int yx = getPossibleMovesDiagonal(this.getDiagonal(start, true));
		if (yx != 0) {
			Point temp = new Point((start.getX() + yx), (start.getY() - yx));
			if (pointIsValid(temp)
					&& pathClear(start, temp, board[start.getY()][start.getX()])) {
				if (isEmptyPosition(temp)) {
					p.add(temp);
				} else {
					if (board[temp.getY()][temp.getX()].compareTo(board[start
							.getY()][start.getX()]) != 0) {
						p.add(temp);
					}
				}
			}
			temp = new Point((start.getX() - yx), (start.getY() + yx));
			if (pointIsValid(temp)
					&& pathClear(start, temp, board[start.getY()][start.getX()])) {
				if (isEmptyPosition(temp)) {
					p.add(temp);
				} else {
					if (board[temp.getY()][temp.getX()].compareTo(board[start
							.getY()][start.getX()]) != 0)
						p.add(temp);
				}
			}
		}
	}

	private void addXYPossibleMoves(final ArrayList<Point> p, final Point start) {
		int xy = getPossibleMovesDiagonal(this.getDiagonal(start, false));
		if (xy != 0) {
			Point temp = new Point((start.getX() + xy), (start.getY() + xy));
			if (pointIsValid(temp)
					&& pathClear(start, temp, board[start.getY()][start.getX()])) {
				if (isEmptyPosition(temp)) {
					p.add(temp);
				} else {
					if (board[temp.getY()][temp.getX()].compareTo(board[start
							.getY()][start.getX()]) != 0)
						p.add(temp);
				}
			}
			temp = new Point((start.getX() - xy), (start.getY() - xy));
			if (pointIsValid(temp)
					&& pathClear(start, temp, board[start.getY()][start.getX()])) {
				if (isEmptyPosition(temp)) {
					p.add(temp);
				} else {
					if (board[temp.getY()][temp.getX()].compareTo(board[start
							.getY()][start.getX()]) != 0)
						p.add(temp);
				}
			}
		}
	}

	private int getPossibleMovesDiagonal(final Point[] diagonal) {
		int counter = 0;
		for (int i = 0; i < diagonal.length; i++) {
			if (this.board[diagonal[i].getY()][diagonal[i].getX()] != null)
				counter++;
		}
		return counter;
	}

	private boolean hasPossibleMoves(final Player p) {
		for (int i = 0; i < p.getCheckers().length; i++) {
			if (getPossibleMovesHelper(p.getCheckers()[i].getPosition()).size() != 0)
				return true;
		}
		return false;
	}

	private Point[] getDiagonal(final Point position, final boolean flag) {
		// if flag = true get [ / ]diagonal
		// if flag = false get[ \ ]diagonal
		ArrayList<Point> points = new ArrayList<Point>();
		points.add(position);
		if (!flag) {
			getDiagonalDownRight(points, new Point(position.getX() + 1,
					position.getY() + 1));
			getDiagonalUpperLeft(points, new Point(position.getX() - 1,
					position.getY() - 1));
		} else {
			getDiagonalUpperRight(points, new Point(position.getX() + 1,
					position.getY() - 1));
			getDiagonalDownLeft(points,
					new Point(position.getX() - 1, position.getY() + 1));
		}
		return arraySorting(points);
	}

	private void getDiagonalDownLeft(final ArrayList<Point> points,
			final Point position) {
		if (position.getX() > size-1|| position.getY() > size-1 || position.getX() < 0
				|| position.getY() < 0)
			return;
		points.add(position);
		getDiagonalDownLeft(points,
				new Point(position.getX() - 1, position.getY() + 1));
	}

	private void getDiagonalUpperRight(final ArrayList<Point> points,
			final Point position) {
		if (position.getX() > size-1 || position.getY() > size-1 || position.getX() < 0
				|| position.getY() < 0)
			return;
		points.add(position);
		getDiagonalUpperRight(points,
				new Point(position.getX() + 1, position.getY() - 1));
	}

	private void getDiagonalDownRight(final ArrayList<Point> points,
			final Point position) {
		if (position.getX() > size-1 || position.getY() > size-1 || position.getX() < 0
				|| position.getY() < 0)
			return;
		points.add(position);
		getDiagonalDownRight(points,
				new Point(position.getX() + 1, position.getY() + 1));
	}

	private void getDiagonalUpperLeft(final ArrayList<Point> points,
			final Point position) {
		if (position.getX() > size-1 || position.getY() > size-1 || position.getX() < 0
				|| position.getY() < 0)
			return;
		points.add(position);
		getDiagonalUpperLeft(points,
				new Point(position.getX() - 1, position.getY() - 1));
	}

	public Player getPlayerTurn() {
		if (getTurn() == 1) {
			return this.firstPlayer;
		} else {
			if (getTurn() == 2) {
				return this.secondPlayer;
			} else
				return null;
		}
	}

	private boolean pathClear(final Point start, final Point end, final Player p) {
		if (start.getX() == end.getX()) {
			return this.yPathClear(start, end, p);
		} else {
			if (start.getY() == end.getY()) {
				return xPathClear(start, end, p);
			} else {
				boolean flag = (start.getX() - end.getX())
						* (start.getY() - end.getY()) < 0;
				if (!(diagonalPathClear(getDiagonal(start, flag), flag, start,
						end, p)))
					return false;
			}
		}
		return true;
	}

	private boolean yPathClear(final Point start, final Point end, Player p) {
		if (end.getY() > start.getY()) {
			for (int i = start.getY(); i < end.getY(); i++) {
				if (!(isEmptyPosition(new Point(start.getX(), i)))) {
					if (this.board[i][start.getX()].compareTo(p) != 0)
						return false;
				}
			}
		} else {
			if (start.getY() > end.getY()) {
				for (int i = end.getY() + 1; i < start.getY(); i++) {
					if (!(isEmptyPosition(new Point(start.getX(), i)))) {
						if (this.board[i][end.getX()].compareTo(p) != 0)
							return false;
					}
				}
			}
		}
		return true;
	}

	private boolean xPathClear(final Point start, final Point end,
			final Player p) {
		if (start.getX() < end.getX()) {
			for (int i = start.getX(); i < end.getX(); i++) {
				if (!(isEmptyPosition(new Point(i, start.getY())))) {
					if (this.board[start.getY()][i].compareTo(p) != 0)
						return false;
				}
			}
		} else {
			if (start.getX() > end.getX()) {
				for (int i = start.getX(); i > end.getX(); i--) {
					if (!(isEmptyPosition(new Point(i, start.getY())))) {
						if (this.board[start.getY()][i].compareTo(p) != 0)
							return false;
					}
				}
			}
		}
		return true;
	}

	private boolean diagonalPathClear(final Point[] points, final boolean flag,
			final Point start, final Point end, final Player p) {
		int s = getInArrayPosition(points, start);
		int e = getInArrayPosition(points, end);
		Point temp = new Point(end.getX(), end.getY());
		if (Math.abs(e - s) != 1) {
			if (!flag) {
				if (e < s) {
					temp = new Point(temp.getX() + 1, temp.getY() + 1);
					for (int i = s; i > e && this.pointIsValid(temp); i--) {
						if (!(isEmptyPosition(temp))
								&& board[temp.getY()][temp.getX()].compareTo(p) != 0) {
							return false;
						}
						temp = new Point(temp.getX() + 1, temp.getY() + 1);
					}
				} else {
					temp = new Point(start.getX(), start.getY());
					for (int i = s; i < e && this.pointIsValid(temp); i++) {
						if (!(isEmptyPosition(temp))
								&& board[temp.getY()][temp.getX()].compareTo(p) != 0) {
							return false;
						}
						temp = new Point(temp.getX() + 1, temp.getY() + 1);
					}
				}
			} else {
				if (e < s) {
					temp = new Point(temp.getX() + 1, temp.getY() - 1);
					for (int i = e; i < s && this.pointIsValid(temp); i++) {
						if (!(isEmptyPosition(temp))
								&& board[temp.getY()][temp.getX()].compareTo(p) != 0) {
							return false;
						}
						temp = new Point(temp.getX() + 1, temp.getY() - 1);
					}
				} else {
					temp = new Point(temp.getX() - 1, temp.getY() + 1);
					for (int i = s; i < e && this.pointIsValid(temp); i++) {
						if (!(isEmptyPosition(temp))
								&& board[temp.getY()][temp.getX()].compareTo(p) != 0) {
							return false;
						}
						temp = new Point(temp.getX() - 1, temp.getY() + 1);
					}
				}
			}
		}
		return true;
	}

	public boolean isEmptyPosition(final Point p) {
		if (this.board[p.getY()][p.getX()] != null)
			return false;
		return true;
	}

	private boolean pointIsValid(final Point p) {
		if (p.getX() > size-1 || p.getY() > size-1 || p.getX() < 0 || p.getY() < 0)
			return false;
		return true;
	}

	private int getInArrayPosition(final Point[] points, final Point p) {
		for (int i = 0; i < points.length; i++) {
			if (points[i].compareTo(p) == 0)
				return i;
		}
		return -1;
	}

	private static Point[] arraySorting(final ArrayList<Point> array) {
		Point[] array1 = new Point[array.size()];
		for (int i = 0; i < array1.length; i++)
			array1[i] = array.get(i);
		for (int position1 = 0; position1 <= array1.length; position1++) {
			for (int position2 = position1 + 1; position2 < array1.length; position2++) {
				if (array1[position1].getX() > array1[position2].getX()) {
					Point temp = array1[position1];
					array1[position1] = array1[position2];
					array1[position2] = temp;
				}
			}
		}
		return array1;
	}

	public int compareTo(Board b) {
		if (b.getName().equalsIgnoreCase(this.name))
			return 0;
		return -1;
	}

	public String getName() {
		return this.name;
	}

	public boolean equals(Board b) {
		if (this.compareTo(b) == 0) {
			return true;
		}
		return false;
	}

	public boolean equal(Board b) {
		if (this.compareTo(b) == 0) {
			return true;
		}
		return false;
	}

	public void print() {
		System.out.println("\n----------start of" + this.name
				+ "'s descreption--------");

		System.out.print("\n");
		for (int i = 0; i < this.size; i++) {
			System.out.println();
			for (int j = 0; j < this.size; j++) {
				if (this.board[i][j] != null) {
					if (this.board[i][j].getChecker(new Point(j, i)).getColor() != null) {
						System.out.print(this.board[i][j].getChecker(new Point(
								j, i)));
					}
				} else {
					System.out.print("[ " + new Point(j, i).draw() + " ]");
				}
				System.out.print("--");
			}
		}
		System.out.println("\n----------end of" + this.name
				+ "'s descreption--------");

	}
}
